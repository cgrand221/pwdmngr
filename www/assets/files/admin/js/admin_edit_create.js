


function addTagFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button type="button" class="btn btn-danger"><i class="fa fa-trash"></i> ' + $($tagFormLi).closest("ul").data('delete-button') + '</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function (e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}

function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<span class="subform"></span>').append(newForm);
    $newLinkLi.before($newFormLi);
    $newFormLi.find('select').select2();

    // add a delete link to the new form
    addTagFormDeleteLink($newFormLi);
}

$(document).ready(function () {
    $('ul.tags').each(function () {
        var $collectionHolder = $(this);

        // setup an "add a tag" link
        var $addTagButton = $('<button type="button" class="btn btn-success"><i class="fa fa-plus"></i> ' + $collectionHolder.data('add-button') + '</button>');
        var $newLinkLi = $('<span class="subform"></span>').append($addTagButton);

        // add a delete link to all of the existing tag form li elements
        $collectionHolder.find('span').each(function () {
            addTagFormDeleteLink($(this));
        });

        // add the "add a tag" anchor and li to the tags ul
        $(this).append($newLinkLi);

        // count the current form inputs we have (e.g. 2), use that as the new
        // index when inserting a new item (e.g. 2)
        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addTagButton.on('click', function (e) {
            // add a new tag form (see next code block)
            addTagForm($collectionHolder, $newLinkLi);
        });
    });

    $(document).ready(function () {
        $('.select2').select2();
    });

    $('#generate-password').click(function () {
        $('#password-generator').toggle();
    });

    function windowcryptogetRandomValues(valeurs, nb_essai) {
        /*var i;
         if (nb_essai == 0){
         valeurs[0] = 52;//KO
         for (i=1;i<valeurs.length;i++){
         valeurs[i] = 41;//OK
         }
         
         }
         else if (nb_essai == 1){
         valeurs[0] = 51;//OK
         for (i=1;i<valeurs.length;i++){
         valeurs[i] = 42;//KO
         }
         }
         else{
         valeurs[0] = 51;//OK
         for (i=1;i<valeurs.length;i++){
         valeurs[i] = 41;//OK
         }
         }*/


        window.crypto.getRandomValues(valeurs);
    }


    function randomString(length, charsetpremier, charsetensuite)
    {

        var i;
        var result = "";
        var isOpera = Object.prototype.toString.call(window.opera) == '[object Opera]';
        if (window.crypto && window.crypto.getRandomValues)
        {
            var max_valeur = 0;
            for (i = 0; i < Uint32Array.BYTES_PER_ELEMENT; i++) {
                max_valeur = max_valeur << 8 | 0xff;
            }
            max_valeur = max_valeur >>> 0;

            var restepremier = (max_valeur % charsetpremier.length);
            var resteensuite = (max_valeur % charsetensuite.length);

            maxvaleurpremier = max_valeur - restepremier;
            maxvaleurensuite = max_valeur - resteensuite;

            //maxvaleurpremier = 52;
            //maxvaleurensuite = 42;

            var tab_tmp = new Array();
            for (i = 0; i < length; i++) {
                tab_tmp[i] = 0;
            }
            var valeurs = new Uint32Array(tab_tmp);


            //for uniform distribution we have to remove numbers which are superior to limits maxvaleurpremier and maxvaleurensuite
            var regenerer = true;
            var nb_essai = 0;
            while ((regenerer) && (nb_essai < 100)) {
                windowcryptogetRandomValues(valeurs, nb_essai);
                regenerer = (valeurs[0] >= maxvaleurpremier);
                i = 1;
                while ((i < length) && (!regenerer)) {
                    regenerer = (valeurs[i] >= maxvaleurensuite);
                    i++;
                }
                //console.log(valeurs);
                nb_essai++;
            }
            result += charsetpremier[valeurs[0] % charsetpremier.length];
            for (i = 1; i < length; i++)
            {
                result += charsetensuite[valeurs[i] % charsetensuite.length];
            }
            return result;
        } else if (isOpera)//Opera's Math.random is secure, see http://lists.w3.org/Archives/Public/public-webcrypto/2013Jan/0063.html
        {
            result += charsetpremier[Math.floor(Math.random() * charsetpremier.length)];
            for (i = 1; i < length; i++)
            {
                result += charsetensuite[Math.floor(Math.random() * charsetensuite.length)];
            }
            return result;
        } else
            throw new Error("Your browser sucks and can't generate secure random numbers");
    }


    function composerMotPasse(length, NombreEnPremier, MinusculeEnPremier, MajusculeEnPremier, AutreEnPremier, NombreEnsuite, MinusculeEnsuite, MajusculeEnsuite, AutreEnsuite)
    {
        var LesChiffres = "0123456789";
        var LesMinuscules = "abcdefghijklmnopqrstuvwxyz";
        var LesMajuscules = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var LesAutres = "`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/? ";

        var charsetpremier = ""
        if (NombreEnPremier == true)
            charsetpremier += LesChiffres;
        if (MinusculeEnPremier == true)
            charsetpremier += LesMinuscules;
        if (MajusculeEnPremier == true)
            charsetpremier += LesMajuscules;
        if (AutreEnPremier == true)
            charsetpremier += LesAutres;

        var charsetensuite = ""
        if (NombreEnsuite == true)
            charsetensuite += LesChiffres;
        if (MinusculeEnsuite == true)
            charsetensuite += LesMinuscules;
        if (MajusculeEnsuite == true)
            charsetensuite += LesMajuscules;
        if (AutreEnsuite == true)
            charsetensuite += LesAutres;

        return randomString(length, charsetpremier, charsetensuite)
    }

    function generer()
    {
        $('#info_partagee_infoPartageeClair_password').val(
            composerMotPasse
            (
                $('#LongMotPasse').val(),
                $('#NombreEnPremier').is(':checked'),
                $('#MinusculeEnPremier').is(':checked'),
                $('#MajusculeEnPremier').is(':checked'),
                $('#AutreEnPremier').is(':checked'),
                $('#NombreEnsuite').is(':checked'),
                $('#MinusculeEnsuite').is(':checked'),
                $('#MajusculeEnsuite').is(':checked'),
                $('#AutreEnsuite').is(':checked')
            )
        );
        return false;
    }
    $('#generer').click(generer);

});