
$(function () {
    $(".dataTable").DataTable();
    $('[data-toggle="tooltip"]').tooltip();

    function clickCopy() {
        var idToCopy = '#' + $(this).data('copy');
        var cutTextarea = document.querySelector(idToCopy);
        console.log($(idToCopy).val());
        cutTextarea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Cutting text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to cut');
        }
    }
    $('.copy').click(clickCopy);
    $('.show-hide').click(function(){
        var element = $(this);
        var idShowHideClair = '#'+element.data('show-hide-clair');
        var idShowHideMasque = '#'+element.data('show-hide-masque');
        var idShowHideDe = '#'+element.data('show-hide-de');
        
        var showHideClair = $(idShowHideClair);
        var showHideMasque = $(idShowHideMasque);
        var showHideDe = $(idShowHideDe);
        
        if(showHideMasque.is(":visible")){// affiche le clair
            element.tooltip('hide').attr('data-original-title', element.data('masquer')).tooltip('show');
            element.html('<i class="fa fa-lock"></i>');
            showHideMasque.hide();
            showHideDe.show();
            showHideClair.css('position', 'relative');
            showHideClair.css('top', '0px');            
        }
        else{
            element.tooltip('hide').attr('data-original-title', element.data('voir')).tooltip('show');
            element.html('<i class="fa fa-eye"></i>');
            showHideMasque.show();
            showHideDe.hide();
            showHideClair.css('position', 'absolute');
            showHideClair.css('top', '-100px');
            $('#password-generator').hide();
        }
    });

});
            