/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../files/admin/scss/app.scss'); 
 
$ = require('jquery');
jQuery = $;
require('bootstrap');
require('adminlte');
require('datatables');

require('datatables.net-bs');
require('select2');
require('icheck'); 
require('ionicons');


 