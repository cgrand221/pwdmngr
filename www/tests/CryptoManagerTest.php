<?php

namespace App\Tests\Util;

use App\Service\CryptoManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\UtilisateurGroupeRepository;
use App\Repository\InfoPartageeGroupeRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Utilisateur;

class CryptoManagerTest extends TestCase
{
    /**
     * @var CryptoManager $cryptoManager
     */
    private $cryptoManager;
    
    public function setUp() {
        parent::setUp();
        
        $this->cryptoManager = new CryptoManager(
            $this->getMockBuilder(ParameterBagInterface::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(SerializerInterface::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(UserPasswordEncoderInterface::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(UtilisateurGroupeRepository::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(InfoPartageeGroupeRepository::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(EntityManagerInterface::class)->disableOriginalConstructor()->getMock()                
        );
    }

    public function testCheckActivationCode()
    {
        $activationCode="";
        $hashActivationCode="";
        $this->cryptoManager->generateActivationCodeAndHash($activationCode, $hashActivationCode);
        $check = $this->cryptoManager->checkActivationCode($hashActivationCode, $activationCode);

        $expected = true;
        $this->assertEquals($expected, $check);
    }
    
    public function testRandomStr(){
        $expected = 10;
        $randomStr = $this->cryptoManager->randomStr($expected);
        $length = strlen($randomStr);
        $this->assertEquals($expected, $length);
    }
    
    public function testGenererMotDePasseDerive(){
        $utilisateur = new Utilisateur();
        $plainPassword = $this->cryptoManager->randomStr(10);//mot de passe reel de l'utilisateur
        $privatePassword = $this->cryptoManager->genererMotDePasseDerive($utilisateur, $plainPassword);//on cree le $privatePassword et on affecte le hash à l'utilisateur
        $privatePassword2 = $this->cryptoManager->getMotDePasseDerive( $utilisateur, $plainPassword);//on obtient a nouveau le $privatePassword a partir du mot de passe de l'utilisateur et du hash à l'utilisateur
        $this->assertEquals($privatePassword, $privatePassword2);
        
    }
    
}

