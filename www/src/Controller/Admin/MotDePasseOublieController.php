<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\DataClass\AskExistingMail;
use App\DataClass\PlainPassword;
use App\Form\AskExistingMailType;
use App\Form\PlainPasswordType;
use App\Form\AskCodeHashType;
use App\Service\GeneralManager;
use App\Service\MotDePasseOublieManager;

/**
 * @Route("/mot_de_passe_oublie")
 */
class MotDePasseOublieController extends AbstractController
{
    /**
     * @Route("/mail", name="mot_de_passe_oublie_mail", methods={"GET","POST"})
     * @param Request $request
     * @param MotDePasseOublieManager $motDePasseOublieManager
     * @return Response
     */
    public function mail(Request $request, MotDePasseOublieManager $motDePasseOublieManager): Response
    {
        $motDePasseOublieManager->init();
        
        $askNewMail = new AskExistingMail();
        $form = $this->createForm(AskExistingMailType::class, $askNewMail, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $motDePasseOublieManager->mailOk($askNewMail->getEmail());
            return $this->redirectToRoute("mot_de_passe_oublie_saisie_code");
        }
        
        return $this->render('admin/motDePasseOublie/mail.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/saisie_code", name="mot_de_passe_oublie_saisie_code", methods={"GET","POST"})
     * @param Request $request
     * @param GeneralManager $generalManager
     * @param MotDePasseOublieManager $motDePasseOublieManager
     * @return Response
     */
    public function saisieCode(Request $request, GeneralManager $generalManager, MotDePasseOublieManager $motDePasseOublieManager): Response
    {
        list($recupCompteHash, $recupCompteExpiration, $askCodeHash) = $motDePasseOublieManager->checkAllowSaisieCode();
        if(!$recupCompteHash || !$recupCompteExpiration || !$askCodeHash){
            return $this->redirectToRoute("mot_de_passe_oublie_mail");
        }
        
        $form = $this->createForm(AskCodeHashType::class, $askCodeHash, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $motDePasseOublieManager->saisieCodeOK();
            return $this->redirectToRoute("mot_de_passe_oublie_saisie_mdp");
        }
        
        return $this->render('admin/motDePasseOublie/saisie_code.html.twig', [
            'form' => $form->createView(),
            'temps_actif' => $this->getParameter('mail.check_compte_mail.temps_actif'),
            'date_actif' => $generalManager->formaterDate($recupCompteExpiration, $this->getParameter('mail.check_compte_mail.date_actif')),
        ]);
    }
    
    /**
     * @Route("/saisie_mdp", name="mot_de_passe_oublie_saisie_mdp", methods={"GET","POST"})
     * @param Request $request
     * @param MotDePasseOublieManager $motDePasseOublieManager
     * @return Response
     */
    public function saisieMdp(Request $request, MotDePasseOublieManager $motDePasseOublieManager): Response
    {
        list($utilisateur, $recupCompteVerifOk) = $motDePasseOublieManager->checkAllowSaisieMdp();       
        if(!$utilisateur || !$recupCompteVerifOk){
            return $this->redirectToRoute("mot_de_passe_oublie_mail");
        }
        
        $plainPassword = new PlainPassword();
        $form = $this->createForm(PlainPasswordType::class, $plainPassword, []);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $motDePasseOublieManager->changerMdPAndPersist($utilisateur, $plainPassword->getPassword());            
            $motDePasseOublieManager->saisieMdpOK();
            return $this->redirectToRoute("mot_de_passe_modifie");            
        }
        return $this->render('admin/motDePasseOublie/saisie_mdp.html.twig', [
            'form' => $form->createView(),
            'utilisateur' => $utilisateur,
        ]);
    }
    
    /**
     * @Route("/modifie", name="mot_de_passe_modifie", methods={"GET"})
     * @param Request $request
     * @param MotDePasseOublieManager $motDePasseOublieManager
     * @return Response
     */
    public function modifie(Request $request, MotDePasseOublieManager $motDePasseOublieManager): Response
    {
        $utilisateur = $motDePasseOublieManager->getUtilisateurMdpModifie();
        if(!$utilisateur){
            return $this->redirectToRoute("mot_de_passe_oublie_mail");
        }
        return $this->render('admin/motDePasseOublie/modifie.html.twig', [
            'utilisateur' => $utilisateur,
        ]);
    }
}
