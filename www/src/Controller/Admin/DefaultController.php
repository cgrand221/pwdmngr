<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Service\MenuManager;
use App\Service\CryptoManager;

class DefaultController extends AbstractController {
    
    /**
     * @Route("/admin", name="accueil")
     * @return Response
     */
    public function accueilAction() {
        return $this->render('admin/accueil.html.twig');
    }

    /**
     * @param MenuManager $menuManager
     * @param CryptoManager $cryptoManager
     * @return type
     */
    public function filAriane(MenuManager $menuManager, CryptoManager $cryptoManager) {
        $entityManager = $this->getDoctrine()->getManager();        
        /** @var \Symfony\Component\HttpFoundation\RequestStack $request */
        $request = $this->container->get('request_stack')->getMasterRequest();
        $routeName = $request->get('_route');
        $filAriane = $menuManager->getMenuAriane($routeName);        
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $ipgsToSave = $cryptoManager->restaurerInfoPartageeGroupe($utilisateur, $request->getSession()->get('privatePassword'));        
        $ugs = $cryptoManager->restaurerUtilisateurGroupes($utilisateur, $request->getSession()->get('privatePassword'));        
        return $this->render('admin/ariane.html.twig', [
                    'filAriane' => $filAriane,
                    'route_name' => $routeName,
        ]);
    }

    /**
     * @param MenuManager $menuManager
     * @return type
     */
    public function menu(MenuManager $menuManager) {
        /** @var \Symfony\Component\HttpFoundation\RequestStack $request */
        $request = $this->container->get('request_stack')->getMasterRequest();
        $routeName = $request->get('_route');
        $menuManager->addActiveClasses($routeName);         
        return $this->render('admin/menu.html.twig', [
                    'menus' => $menuManager->getMenus(),
                    'route_name' => $routeName,
        ]);
    }

    public function navbar() {
        return $this->render('admin/navbar.html.twig');
    }

}
