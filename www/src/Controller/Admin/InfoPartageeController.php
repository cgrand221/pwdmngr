<?php

namespace App\Controller\Admin;

use App\Entity\InfoPartagee;
use App\Form\InfoPartageeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\InfoPartageeManager;
use App\Repository\GroupeRepository;

/**
 * @Route("/admin/info_partagee")
 */
class InfoPartageeController extends AbstractController
{
    /**
     * @Route("/", name="info_partagee_index", methods={"GET"})
     * @param Request $request
     * @param InfoPartageeManager $infoPartageeManager
     * @return Response
     */
    public function index(Request $request, InfoPartageeManager $infoPartageeManager): Response
    {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        list($infoPartagees, $infoPartageeClairs, $modificationPbs) = $infoPartageeManager->getIndexInfos($utilisateur, $request->getSession()->get('privatePassword'));
        
        return $this->render('admin/info_partagee/index.html.twig', [
            'info_partagees' => $infoPartagees,
            'info_partagee_clairs' => $infoPartageeClairs,
            'modification_pbs'  => $modificationPbs,
        ]);
    }

    /**
     * @Route("/new", name="info_partagee_new", methods={"GET","POST"})
     * @param Request $request
     * @param GroupeRepository $groupeRepository
     * @param InfoPartageeManager $infoPartageeManager
     * @return Response
     */
    public function newInfoPartagee(Request $request, GroupeRepository $groupeRepository, InfoPartageeManager $infoPartageeManager): Response
    {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $monGroupe = $groupeRepository->findOneBy(['nom' => $utilisateur->getEmail()]);
        if($monGroupe === null){//si je n'ai pas de groupe par defaut => pb
            return $this->redirectToRoute('info_partagee_index');
        }
        
        $infoPartagee = new InfoPartagee();
        $form = $this->createForm(InfoPartageeType::class, $infoPartagee, ['utilisateur' => $utilisateur]);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $infoPartageeManager->newInfoPartageeAndPersist($infoPartagee, $monGroupe, $utilisateur, $request->getSession()->get('privatePassword'), $form->get('infoPartageeClair')->getData());
            
            if($request->get('enregistrer_quitter') !== null){
                return $this->redirectToRoute('info_partagee_index');
            }
            return $this->redirectToRoute('info_partagee_edit', ['id' => $infoPartagee->getId()]);
        }

        return $this->render('admin/info_partagee/edit_new.html.twig', [
            'info_partagee' => $infoPartagee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/show", name="info_partagee_show", methods={"GET"})
     * @param Request $request
     * @param InfoPartagee $infoPartagee
     * @param InfoPartageeManager $infoPartageeManager
     * @return Response
     */
    public function showInfoPartagee(Request $request, InfoPartagee $infoPartagee, InfoPartageeManager $infoPartageeManager): Response
    {
        $infoPartageeClair = $infoPartageeManager->getShowInfo($this->get('security.token_storage')->getToken()->getUser(), $request->getSession()->get('privatePassword'), $infoPartagee);
        if($infoPartageeClair === null){
            return $this->redirectToRoute('info_partagee_index');
        }        
        
        return $this->render('admin/info_partagee/show.html.twig', [
            'info_partagee_clair' => $infoPartageeClair,
            'info_partagee' => $infoPartagee,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="info_partagee_edit", methods={"GET","POST"})
     * @param Request $request
     * @param InfoPartagee $infoPartagee
     * @param InfoPartageeManager $infoPartageeManager
     * @return Response
     */
    public function editInfoPartagee(Request $request, InfoPartagee $infoPartagee, InfoPartageeManager $infoPartageeManager): Response
    {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $infoPartageeClair = $infoPartageeManager->prepareEdit($utilisateur, $request->getSession()->get('privatePassword'), $infoPartagee);
        if(!$infoPartageeClair){
            return $this->redirectToRoute('info_partagee_index');
        }
        $form = $this->createForm(InfoPartageeType::class, $infoPartagee, ['utilisateur' => $utilisateur]);
        $form->get('infoPartageeClair')->setData($infoPartageeClair);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $infoPartageeManager->editAndPersist($infoPartagee, $request->getSession()->get('privatePassword'), $infoPartageeClair, $utilisateur);
            if($request->get('enregistrer_quitter') !== null){
                return $this->redirectToRoute('info_partagee_index');
            }
            return $this->redirectToRoute('info_partagee_edit', ['id' => $infoPartagee->getId()]);
        }

        return $this->render('admin/info_partagee/edit_new.html.twig', [
            'info_partagee' => $infoPartagee,
            'form' => $form->createView(),
        ]);
    }

    
}
