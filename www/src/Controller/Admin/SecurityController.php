<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Utilisateur;
use App\Form\ActivationCompteType;
use App\Service\GeneralManager;
use App\DataClass\AskNewCode;
use App\Form\AskNewCodeType;
use App\Service\SecurityManager;

class SecurityController extends AbstractController {

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response {       
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('admin/security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param SecurityManager $securityManager
     * @return Response
     */
    public function register(Request $request, SecurityManager $securityManager): Response {
        $utilisateur = new Utilisateur();
        $form = $this->createForm(RegistrationFormType::class, $utilisateur);
        $form->handleRequest($request);   
        if ($form->isSubmitted() && $form->isValid()) {
            $securityManager->register($utilisateur, $form->get('plainPassword')->getData());
            return $this->redirectToRoute('app_register_after');
        }

        return $this->render('admin/security/register.html.twig', [
                    'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ask_new_code", name="app_ask_new_code")  
     * @param Request $request
     * @param SecurityManager $securityManager
     * @return Response
     */
    public function askNewCode(Request $request, SecurityManager $securityManager): Response {     
        /** 
         * @var Utilisateur|null $utilisateur 
         * @var AskNewCode $askNewCode 
         */
        list($utilisateur, $askNewCode) = $securityManager->prepareAskNewCode();
        $form = $this->createForm(AskNewCodeType::class, $askNewCode);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $securityManager->askNewCodeSubmit($askNewCode);
            return $this->redirectToRoute('app_register_after');
        }
        return $this->render('admin/security/ask_new_code.html.twig', [
                    'ask_new_code_form' => $form->createView(),
        ]);
    }

   /**
    * @Route("/register_after", name="app_register_after")
    * @param Request $request
    * @param GeneralManager $generalManager
    * @return Response
    */
    public function registerAfter(Request $request, GeneralManager $generalManager) {
        /** @var Utilisateur|null $utilisateur */
        $utilisateur = $request->getSession()->get('register_after_user', null);

        if ($utilisateur !== null) {
            return $this->render('admin/security/register_after.html.twig', [
                'utilisateur' => $utilisateur,
                'temps_actif' => $this->getParameter('mail.check_compte_mail.temps_actif'),
                'date_actif' =>  $generalManager->formaterDate($utilisateur->getDateExpirationActivation(), $this->getParameter('mail.check_compte_mail.date_actif')),
            ]);
        }
        return $this->redirectToRoute("accueilfront");
    }

    /**
     * @Route("/activation_compte_code", name="activation_compte_code")
     * @param Request $request
     * @param SecurityManager $securityManager
     * @return Response
     */
    public function activationCompteCode(Request $request, SecurityManager $securityManager) {
        $activationCompte = $securityManager->prepareActivationCompteCode();       
        $form = $this->createForm(ActivationCompteType::class, $activationCompte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $securityManager->activationCompteCodeSubmit($activationCompte->getEmail());
            return $this->redirectToRoute("compte_actif");
        }
        return $this->render('admin/security/activation_compte_code.html.twig', [
                    'activationCompteForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/compte_actif", name="compte_actif")
     * @param Request $request
     * @return Response
     */
    public function compteActif(Request $request) {
        $utilisateur = $request->getSession()->get('register_after_user', null);
        if ($utilisateur) {
            return $this->render('admin/security/compte_actif.html.twig', [
                        'utilisateur' => $utilisateur,
            ]);
        }
        return $this->redirectToRoute("activation_compte_code");
    }

}
