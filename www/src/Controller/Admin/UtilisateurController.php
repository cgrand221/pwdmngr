<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\DataClass\UtilisateurPasswordCheck;
use App\Form\UtilisateurPasswordCheckType;
use App\Form\UtilisateurInfoType;
use App\Form\AskCodeHashType;
use App\Form\AskNewMailType;
use App\Service\UtilisateurManager;
use App\Service\GeneralManager;

/**
 * @Route("/admin/utilisateur")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/", name="utilisateur_resume", methods={"GET"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurResume(Request $request, UtilisateurManager $utilisateurManager): Response
    {
        $session = $request->getSession();
        $utilisateurManager->init();        
        return $this->render('admin/utilisateur/utilisateur_resume.html.twig', [
            'utilisateur' => $this->get('security.token_storage')->getToken()->getUser(),
        ]);
    }
   
   /**
    * @Route("/edit", name="utilisateur_edit", methods={"GET","POST"})
    * @param Request $request
    * @param UtilisateurManager $utilisateurManager
    * @return Response
    */
    public function utilisateurEdit(Request $request, UtilisateurManager $utilisateurManager): Response
    {
        $utilisateurPasswordCheck = new UtilisateurPasswordCheck();
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $utilisateurPasswordCheck->setUtilisateur($utilisateur);
        $form = $this->createForm(UtilisateurPasswordCheckType::class, $utilisateurPasswordCheck, []);
        $form->handleRequest($request);        
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditSubmit();            
            return $this->redirectToRoute("utilisateur_edit_allow");
        }        
        return $this->render('admin/utilisateur/edit.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/edit_forbidden", name="utilisateur_edit_forbidden", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function utilisateurEditForbidden(Request $request): Response
    {
        return $this->render('admin/utilisateur/edit_forbidden.html.twig', [
        ]);
    }
    
    /**
     * @Route("/edit_allow", name="utilisateur_edit_allow", methods={"GET","POST"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurEditAllow(Request $request, UtilisateurManager $utilisateurManager): Response
    {
        if(!$utilisateurManager->checkUtilisateurEditAllow()){
            return $this->redirectToRoute("utilisateur_edit_forbidden");
        }        
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(UtilisateurInfoType::class, $utilisateur, []);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditsubmitAndPersist($utilisateur, $form->get('plainPassword')->getData(), $request->getSession()->get('privatePassword'));            
            $request->getSession()->set('timeLimitUtilisateurEdit', null);
            return $this->redirectToRoute("utilisateur_resume");
        }        
        return $this->render('admin/utilisateur/edit_allow.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/edit_cancel", name="utilisateur_edit_cancel", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function utilisateurEditCancel(Request $request): Response
    {
        $request->getSession()->set('timeLimitUtilisateurEdit', null);
        return $this->redirectToRoute("utilisateur_resume");
    }
    
    /**
     * @Route("/edit_email", name="utilisateur_edit_email", methods={"GET","POST"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurEditEmail(Request $request, UtilisateurManager $utilisateurManager): Response
    {
        $utilisateurPasswordCheck = new UtilisateurPasswordCheck();
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        $utilisateurPasswordCheck->setUtilisateur($utilisateur);
        $form = $this->createForm(UtilisateurPasswordCheckType::class, $utilisateurPasswordCheck, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditEmailSubmitAndPersist($utilisateur);
            return $this->redirectToRoute("utilisateur_edit_email_allow_1");
        }
        
        return $this->render('admin/utilisateur/edit_email.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/edit_email_allow_1", name="utilisateur_edit_email_allow_1", methods={"GET","POST"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurEditMailAllow1(Request $request, UtilisateurManager $utilisateurManager, GeneralManager $generalManager): Response
    {
        list($askCodeHash, $utilisateurEditEmailCodeExpiration1) = $utilisateurManager->prepareUtilisateurEditMailAllow1();       
        $form = $this->createForm(AskCodeHashType::class, $askCodeHash, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditMailAllow1Submit();
            return $this->redirectToRoute("utilisateur_edit_email_allow_2");
        }
       
        return $this->render('admin/utilisateur/edit_email_allow_1.html.twig', [
            'form' => $form->createView(),
            'temps_actif' => $this->getParameter('mail.check_compte_mail.temps_actif'),
            'date_actif' => $generalManager->formaterDate($utilisateurEditEmailCodeExpiration1, $this->getParameter('mail.check_compte_mail.date_actif')),
        ]);
    }
    
    /**
     * @Route("/edit_email_allow_2", name="utilisateur_edit_email_allow_2", methods={"GET","POST"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurEditMailAllow2(Request $request, UtilisateurManager $utilisateurManager): Response
    {
        $askNewMail = $utilisateurManager->prepareUtilisateurEditMailAllow2();
        $form = $this->createForm(AskNewMailType::class, $askNewMail, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditMailAllow2Submit($askNewMail->getEmail());
            return $this->redirectToRoute("utilisateur_edit_email_allow_3");
        }
        return $this->render('admin/utilisateur/edit_email_allow_2.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
     
    /**
     * @Route("/edit_email_allow_3", name="utilisateur_edit_email_allow_3", methods={"GET","POST"})
     * @param Request $request
     * @param UtilisateurManager $utilisateurManager
     * @return Response
     */
    public function utilisateurEditMailAllow3(Request $request, UtilisateurManager $utilisateurManager, GeneralManager $generalManager): Response
    {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        list($askCodeHash, $utilisateurEditEmailCodeExpiration2) = $utilisateurManager->prepareUtilisateurEditMailAllow3($utilisateur);
       
        $form = $this->createForm(AskCodeHashType::class, $askCodeHash, []);
        $form->handleRequest($request);        
        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateurManager->utilisateurEditMailAllow3SubmitAndPersist($utilisateur);
            return $this->redirectToRoute("utilisateur_resume");
        }
       
        return $this->render('admin/utilisateur/edit_email_allow_3.html.twig', [
            'form' => $form->createView(),
            'temps_actif' => $this->getParameter('mail.check_compte_mail.temps_actif'),
            'date_actif' => $generalManager->formaterDate($utilisateurEditEmailCodeExpiration2, $this->getParameter('mail.check_compte_mail.date_actif')),
        ]);
    }
}
