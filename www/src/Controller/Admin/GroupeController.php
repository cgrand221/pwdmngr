<?php

namespace App\Controller\Admin;

use App\Entity\Groupe;
use App\Form\GroupeType;
use App\Repository\GroupeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\GroupeManager;
/**
 * @Route("/admin/groupes")
 */
class GroupeController extends AbstractController
{
    /**
     * @Route("/", name="groupe_index", methods={"GET"})
     * @param GroupeRepository $groupeRepository
     * @return Response
     */
    public function index(GroupeRepository $groupeRepository): Response
    {
        return $this->render('admin/groupe/index.html.twig', [
            'groupes' => $groupeRepository->getMesGroupesCrees($this->get('security.token_storage')->getToken()->getUser(), false),
        ]);
    }
    
    /**
     * @Route("/membre_de", name="groupe_membre_de_list", methods={"GET"})
     * @param GroupeRepository $groupeRepository
     * @return Response
     */
    public function membreDe(GroupeRepository $groupeRepository): Response
    {
        return $this->render('admin/groupe/membre_de.html.twig', [
            'groupes' => $groupeRepository->getMesGroupes($this->get('security.token_storage')->getToken()->getUser(), true, true, false),
        ]);
    }

    /**
     * @Route("/new", name="groupe_new", methods={"GET","POST"})
     * @param Request $request
     * @param GroupeManager $groupeManager
     * @return Response
     */
    public function newGroupe(Request $request, GroupeManager $groupeManager): Response
    {
        $groupe = new Groupe();
        $form = $this->createForm(GroupeType::class, $groupe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {            
            $groupeManager->newGroupeAndPersist(
                    $this->get('security.token_storage')->getToken()->getUser(), 
                    $groupe, 
                    $request->getSession()->get('privatePassword')
                );            
            if($request->get('enregistrer_quitter') !== null){
                return $this->redirectToRoute('groupe_index');
            }
            return $this->redirectToRoute('groupe_edit', ['id' => $groupe->getId()]);
        } 
        return $this->render('admin/groupe/edit_new.html.twig', [
            'groupe' => $groupe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="groupe_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Groupe $groupe
     * @param GroupeManager $groupeManager
     * @return Response
     */
    public function editGroupe(Request $request, Groupe $groupe, GroupeManager $groupeManager): Response
    {
        $utilisateur = $this->get('security.token_storage')->getToken()->getUser();
        if(!$groupeManager->prepareEdit($utilisateur, $groupe)){
            return $this->redirectToRoute('groupe_index');
        }        
        $form = $this->createForm(GroupeType::class, $groupe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $groupeManager->editAndPersist($utilisateur, $groupe, $request->getSession()->get('privatePassword'));
            if($request->get('enregistrer_quitter') !== null){
                return $this->redirectToRoute('groupe_index');
            }
            return $this->redirectToRoute('groupe_edit', ['id' => $groupe->getId()]);
        }
        return $this->render('admin/groupe/edit_new.html.twig', [
            'groupe' => $groupe,
            'form' => $form->createView(),
        ]);
    }
}
