<?php

namespace App\Controller\Mail;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MenuManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\MailManager;
use App\Repository\UtilisateurRepository;
/**
 * @Route("/mails")
 */
class ListMailController extends AbstractController {

    /**
     * @Route("/creer_compte", name="creer_compte_mail")
     */
    public function creerCompteMailAction(MailManager $mailManager, UtilisateurRepository $utilisateurRepository) {
        $utilisateur = $utilisateurRepository->find(9);
        $mailManager->sendMailActivationCompte($utilisateur);
        
        return $this->render('mail/test.html.twig', [
                    'prenom' => 'prenom',
                    'nom' => 'nom',
                    'code' => '123456',
        ]);
    }

    /**
     * @Route("/recuperer_compte", name="recuperer_compte_mail")
     */
    public function recupererCompteMailAction() {
        return $this->render('mail/recuperer_compte_mail.html.twig', [
                    'prenom' => 'prenom',
                    'nom' => 'nom',
                    'code' => '123456',
        ]);
    }

    /**
     * @Route("/changer_mot_de_passe", name="changer_mot_de_passe_mail")
     */
    public function changerMotDePasseMailAction() {
        return $this->render('mail/changer_mot_de_passe_mail.html.twig', [
                    'prenom' => 'prenom',
                    'nom' => 'nom',
                    'code' => '123456',
        ]);
    }

}
