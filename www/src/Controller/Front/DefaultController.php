<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MenuManager;

class DefaultController extends AbstractController {

    /**
     * Page d'accueil front
     *
     * @Route("/", name="accueilfront")
     */
    public function accueilAction() {
        
        return $this->render('front/index.html.twig', [
            'temps_actif' => $this->getParameter('mail.check_compte_mail.temps_actif'),
        ]);
    }

    
   
}
