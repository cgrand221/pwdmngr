<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\Utilisateur;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailManager {

    /**
     * @var GeneralManager $generalManager
     */
    private $generalManager;

    /**
     * @var Twig_Environment $twig
     */
    private $twig;

    /**
     * @var Swift_Mailer $twig
     */
    private $mailer;

    /**
     * @var RouterInterface $routing
     */
    private $routing;

    /**
     * @var string|null $domain
     */
    private $domain;

    /**
     * @var ParameterBagInterface $params
     */
    private $params;

    /**
     * @param \Twig_Environment $twig
     */
    public function __construct(GeneralManager $generalManager, \Twig_Environment $twig, \Swift_Mailer $mailer, RouterInterface $routing, ParameterBagInterface $params) {
        $this->generalManager = $generalManager;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->routing = $routing;
        $this->params = $params;
        $this->domain = null;
    }

    private function generateDomainForMail() {
        if ($this->domain === null) {
            $url = $this->routing->generate('accueilfront', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $pos = strpos($url, "//");
            if ($pos !== false) {
                $url = substr($url, $pos + 2);
            }
            $tabUrl = explode("/", $url);
            $url = $tabUrl[0];
            $tabUrl = explode(".", $url);
            $pos = array_search("www", $tabUrl);
            if ($pos === 0) {
                unset($tabUrl[0]);
            }
            $this->domain = implode(".", $tabUrl);
        }
        return $this->domain;
    }

    /**
     * @param Utilisateur $utilisateur
     * @param string $activationCode
     * @param \DateTime $dateExpirationActivation
     */
    public function sendMailActivationCompte(Utilisateur $utilisateur, $activationCode, $dateExpirationActivation) {
        $domain = $this->generateDomainForMail();

        $tempsActif = $this->params->get('mail.check_compte_mail.temps_actif');
        $dateActif = $this->generalManager->formaterDate($dateExpirationActivation, $this->params->get('mail.check_compte_mail.date_actif'));

        $message = (new \Swift_Message('Password MANAGER - création de compte'))
                ->setFrom('noreply@' . $domain)
                ->setTo($utilisateur->getEmail())
                ->setBody(
                $this->twig->render(
                        'mail/creer_compte_mail.html.twig', [
                    'prenom' => $utilisateur->getPrenom(),
                    'nom' => $utilisateur->getNom(),
                    'code' => $activationCode,
                    'temps_actif' => $tempsActif,
                    'date_actif' => $dateActif,
                        ]
                ), 'text/html'
                )
        ;

        $this->mailer->send($message);
    }

    /**
     * @param Utilisateur $utilisateur
     */
    public function sendMailChangementMotDePasse(Utilisateur $utilisateur) {
        $domain = $this->generateDomainForMail();


        $message = (new \Swift_Message('Password MANAGER - changement de mot de passe'))
                ->setFrom('noreply@' . $domain)
                ->setTo($utilisateur->getEmail())
                ->setBody(
                $this->twig->render(
                        'mail/changer_mot_de_passe_mail.html.twig', [
                    'prenom' => $utilisateur->getPrenom(),
                    'nom' => $utilisateur->getNom(),
                        ]
                ), 'text/html'
                )
        ;

        $this->mailer->send($message);
    }

    
    /**
     * @param Utilisateur $utilisateur
     * @param string $activationCode
     * @param \DateTime $dateExpirationActivation
     */
    public function sendMailChangementEmail1(Utilisateur $utilisateur, $activationCode, $dateExpirationActivation) {
        $domain = $this->generateDomainForMail();

        $tempsActif = $this->params->get('mail.check_compte_mail.temps_actif');
        $dateActif = $this->generalManager->formaterDate($dateExpirationActivation, $this->params->get('mail.check_compte_mail.date_actif'));
        $domain = $this->generateDomainForMail();


        $message = (new \Swift_Message('Password MANAGER - changement de mail'))
                ->setFrom('noreply@' . $domain)
                ->setTo($utilisateur->getEmail())
                ->setBody(
                $this->twig->render(
                        'mail/changer_mail_1.html.twig', [
                        'prenom' => $utilisateur->getPrenom(),
                        'nom' => $utilisateur->getNom(),
                        'code' => $activationCode,
                        'temps_actif' => $tempsActif,
                        'date_actif' => $dateActif,
                        ]
                ), 'text/html'
                )
        ;

        $this->mailer->send($message);
    }
    
    /**
     * @param string $email
     * @param string $activationCode
     * @param \DateTime $dateExpirationActivation
     */
    public function sendMailChangementEmail2($email, $activationCode, $dateExpirationActivation) {
        $domain = $this->generateDomainForMail();

        $tempsActif = $this->params->get('mail.check_compte_mail.temps_actif');
        $dateActif = $this->generalManager->formaterDate($dateExpirationActivation, $this->params->get('mail.check_compte_mail.date_actif'));
        $domain = $this->generateDomainForMail();


        $message = (new \Swift_Message('Password MANAGER - changement de mail'))
                ->setFrom('noreply@' . $domain)
                ->setTo($email)
                ->setBody(
                $this->twig->render(
                        'mail/changer_mail_2.html.twig', [
                        
                        'code' => $activationCode,
                        'temps_actif' => $tempsActif,
                        'date_actif' => $dateActif,
                        ]
                ), 'text/html'
                )
        ;

        $this->mailer->send($message);
    }
    
    
   /**
     * @param Utilisateur $utilisateur
     * @param string $activationCode
     * @param \DateTime $dateExpirationActivation
     */
    public function sendMailRecuperationCompte(Utilisateur $utilisateur, $code, $dateExpirationActivation) {
        $domain = $this->generateDomainForMail();

        $tempsActif = $this->params->get('mail.check_compte_mail.temps_actif');
        $dateActif = $this->generalManager->formaterDate($dateExpirationActivation, $this->params->get('mail.check_compte_mail.date_actif'));
        $domain = $this->generateDomainForMail();


        $message = (new \Swift_Message('Password MANAGER - récupération de compte'))
                ->setFrom('noreply@' . $domain)
                ->setTo($utilisateur->getEmail())
                ->setBody(
                $this->twig->render(
                        'mail/recuperation_compte.html.twig', [
                            'prenom' => $utilisateur->getPrenom(),
                            'nom' => $utilisateur->getNom(),
                            'code' => $code,
                            'temps_actif' => $tempsActif,
                            'date_actif' => $dateActif,
                        ]
                ), 'text/html'
                )
        ;

        $this->mailer->send($message);
    }
}
