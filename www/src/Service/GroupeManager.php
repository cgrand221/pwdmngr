<?php

namespace App\Service;
use App\Entity\Utilisateur;
use App\Entity\Groupe;
use App\Entity\UtilisateurGroupe;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InfoPartageeGroupeRepository;

class GroupeManager {
    
    /**
     * @var CryptoManager $cryptoManager
     */
    private $cryptoManager;
    
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    
    /**
     * @var InfoPartageeGroupeRepository $infoPartageeGroupeRepository
     */
    private $infoPartageeGroupeRepository;
    
    /**
     * @var UtilisateurGroupe[] $ugs
     */
    private $ugs;
    
    /**
     * @param \App\Service\CryptoManager $cryptoManager
     * @param EntityManagerInterface $em
     */
    public function __construct(CryptoManager $cryptoManager, EntityManagerInterface $em, InfoPartageeGroupeRepository $infoPartageeGroupeRepository){
        $this->cryptoManager = $cryptoManager;
        $this->em = $em;
        $this->infoPartageeGroupeRepository = $infoPartageeGroupeRepository;
        $this->ugs = [];
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param Groupe $groupe
     * @param string $privatePassword
     */
    public function newGroupeAndPersist(Utilisateur $utilisateur, Groupe $groupe, $privatePassword){// 
        $groupe->setProprietaire($utilisateur);

        $ug=null;
        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            if($utilisateurGroupe->getUtilisateur()->getId() == $utilisateur->getId()){
                $ug = $utilisateurGroupe;
                break;
            }
        }
        if(!$ug){
            $ug = new UtilisateurGroupe();
            $groupe->addUtilisateurGroupe($ug);
            $utilisateur->addUtilisateurGroupe($ug);
        }
        
        $this->cryptoManager->setFirstPasswordGroupe($ug, $privatePassword); 
        $this->em->persist($ug);
        $this->em->flush();

        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            if($utilisateurGroupe->getUtilisateur()->getId() != $utilisateur->getId()){
                $this->cryptoManager->setPasswordGroupe($utilisateurGroupe, $utilisateur, $privatePassword);
            }
        }
        $this->em->persist($groupe);
        $this->em->flush();
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param Groupe $groupe
     * @return boolean
     */
    public function prepareEdit(Utilisateur $utilisateur, Groupe $groupe){   
        if(
                ($groupe->getNom() == $utilisateur->getEmail())
                || ($groupe->getProprietaire()->getId() != $utilisateur->getId())
        ){
            return false;
        }
        
        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            $this->ugs[$utilisateurGroupe->getUtilisateur()->getId()] = $utilisateurGroupe;
        }
        $ug = $this->ugs[$utilisateur->getId()];
        if(($ug->getPassword() == null) && ($ug->getPasswordCache() == null)){//le mot de passe du groupe est illisible suite à une récupération de compte, il faut attendre qu'un autre membre du groupe se connecte
            return false;
        }
        return true;
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param Groupe $groupe
     * @param string $privatePassword
     */
    public function editAndPersist(Utilisateur $utilisateur, Groupe $groupe, $privatePassword){
        $ugs2=[];
        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            if(isset($this->ugs[$utilisateurGroupe->getUtilisateur()->getId()]) && !$utilisateurGroupe->getId()){//l'original a disparu et a ete remplace par la copie, or on prefere l'original a la copie
                $ug = $this->ugs[$utilisateurGroupe->getUtilisateur()->getId()];
                $groupe->removeUtilisateurGroupe($utilisateurGroupe);
                $ug->getUtilisateur()->removeUtilisateurGroupe($utilisateurGroupe);
                $groupe->addUtilisateurGroupe($ug);
                $ug->getUtilisateur()->addUtilisateurGroupe($ug);
                $ugs2[$utilisateurGroupe->getUtilisateur()->getId()] = $ug;
            }
            else{
                $ugs2[$utilisateurGroupe->getUtilisateur()->getId()] = $utilisateurGroupe;
            }
        }
        if(!isset($ugs2[$utilisateur->getId()])){// on remet en place l'original si il a disparu
            $groupe->addUtilisateurGroupe($this->ugs[$utilisateur->getId()]);
            $utilisateur->addUtilisateurGroupe($this->ugs[$utilisateur->getId()]);
        }
        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            if(!$utilisateurGroupe->getId()){// on chiffre seulement pour les nouveaux utilisateurGroupe
                $this->cryptoManager->setPasswordGroupe($utilisateurGroupe, $utilisateur, $privatePassword);
            }
        }
        
        //liste des mebres supprimes :
        foreach ($groupe->getUtilisateurGroupes() as $utilisateurGroupe){
            if(isset($this->ugs[$utilisateurGroupe->getUtilisateur()->getId()])){
                unset($this->ugs[$utilisateurGroupe->getUtilisateur()->getId()]);
            }
        }
        
        $ipgs = $this->infoPartageeGroupeRepository->getInfosNonMembre($this->ugs);
        foreach ($ipgs as $ipg){
            $this->em->remove($ipg);
        }
        
        $this->em->persist($groupe);
        $this->em->flush();
    }
}
