<?php

namespace App\Service;
use App\Entity\Utilisateur;
use App\DataClass\AskNewCode;
use App\DataClass\ActivationCompte;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Groupe;
use App\Entity\UtilisateurGroupe;

class SecurityManager {
   
    
    /**
    * @var MailManager $mailManager 
    */
    private $mailManager;
    
    /**
     * @var CryptoManager $cryptoManager 
     */
    private $cryptoManager;
    
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder 
     */
    private $passwordEncoder;
    
    /**
     * @var SessionInterface $session
     */
    private $session;
    
    /**
     * @var UtilisateurRepository $utilisateurRepository
     */
    private $utilisateurRepository;
    
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    
    public function __construct(EntityManagerInterface $em, MailManager $mailManager, CryptoManager $cryptoManager,  UserPasswordEncoderInterface $passwordEncoder, SessionInterface $session, UtilisateurRepository $utilisateurRepository) {
        $this->cryptoManager = $cryptoManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->session = $session;
        $this->mailManager = $mailManager;
        $this->em = $em;  
        $this->utilisateurRepository = $utilisateurRepository;
    }

    /**
     * @param Utilisateur $utilisateur
     * @param type $plainPassword
     */
    public function register(Utilisateur $utilisateur, $plainPassword){
        // encode the plain password
        $utilisateur->setPassword(
                $this->passwordEncoder->encodePassword(
                        $utilisateur, $plainPassword
                )
        );
        $utilisateur->setEtat(Utilisateur::ETAT_EMAIL_NON_VERIFIE);

        $activationCode = $dateExpirationActivation = null;
        $this->cryptoManager->generateHashCodeExpiration($utilisateur, $activationCode, $dateExpirationActivation);

        $groupe = new Groupe();
        $groupe->setProprietaire($utilisateur);
        $groupe->setNom($utilisateur->getEmail());
        $groupe->setEtat(Groupe::ETAT_ACTIF);

        $utilisateurGroupe = new UtilisateurGroupe();
        $utilisateurGroupe->setUtilisateur($utilisateur);
        $utilisateurGroupe->setGroupe($groupe);
        $privatePassword = $this->cryptoManager->genererMotDePasseDerive($utilisateur, $plainPassword);
        $this->cryptoManager->affecterClesRSA($utilisateur, $privatePassword);
        $this->cryptoManager->setFirstPasswordGroupe($utilisateurGroupe, $privatePassword);

        $this->em->persist($groupe);
        $this->em->persist($utilisateurGroupe);
        $this->em->flush();

        $this->mailManager->sendMailActivationCompte($utilisateur, $activationCode, $dateExpirationActivation);

        $this->session->set('register_after_user', $utilisateur);
        // do anything else you need here, like send an email
    }
    
    /**
     * @return array
     */
    public function prepareAskNewCode(){
        $askNewCode = new AskNewCode();
        $utilisateur = $this->session->get('register_after_user', null);
        if ($utilisateur !== null) {
            $askNewCode->setEmail($utilisateur->getEmail());
        }
        return [$utilisateur, $askNewCode];
    }
    
    /**
     * @param AskNewCode $askNewCode
     */
    public function askNewCodeSubmit($askNewCode){
        $utilisateur = $this->utilisateurRepository->findOneBy(['email' => $askNewCode->getEmail(), 'etat' => Utilisateur::ETAT_EMAIL_NON_VERIFIE]);
        if ($utilisateur) {
            $activationCode = $dateExpirationActivation = null;
            $this->cryptoManager->generateHashCodeExpiration($utilisateur, $activationCode, $dateExpirationActivation);           
            $this->em->persist($utilisateur);
            $this->em->flush();
            $this->mailManager->sendMailActivationCompte($utilisateur, $activationCode, $dateExpirationActivation);
            $this->session->set('register_after_user', $utilisateur);
        }
    }
    
    /**
     * @return ActivationCompte
     */
    public function prepareActivationCompteCode(){
         /** @var Utilisateur|null $utilisateur */
        $utilisateur = $this->session->get('register_after_user', null);
        $activationCompte = new ActivationCompte();
        if ($utilisateur) {
            $activationCompte->setEmail($utilisateur->getEmail());
        }
        return $activationCompte;
    }
    
    
    public function activationCompteCodeSubmit($email){
        $utilisateur = $this->utilisateurRepository->findForActivation($email);
        if ($utilisateur) {
            $utilisateur->setEtat(Utilisateur::ETAT_ACTIF);            
            $this->em->persist($utilisateur);
            $this->em->flush();
            $this->session->set('register_after_user', $utilisateur);
        }
    }
}
