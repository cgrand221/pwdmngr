<?php

namespace App\Service;

use App\Entity\Utilisateur;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\UtilisateurGroupe;
use phpseclib\Crypt\RSA;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\Random;
use App\Entity\Groupe;
use Symfony\Component\Serializer\SerializerInterface;
use App\DataClass\InfoPartageeClair;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\UtilisateurGroupeRepository;
use App\Repository\InfoPartageeGroupeRepository;
use Doctrine\ORM\EntityManagerInterface;

class CryptoManager {

    /**
     * @var ParameterBagInterface $params
     */
    private $params;
    
    /**
     * @var SerializerInterface $serializer
     */
    private $serializer;
    
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder
     */
    private $passwordEncoder;
    
    /**
     * @var UtilisateurGroupeRepository $utilisateurGroupeRepository
     */
    private $utilisateurGroupeRepository;
    
    /**
     * @var string[] $cacheMotDePasseGroupe
     */
    private $cacheMotDePasseGroupe;
    
    /**
     * @var UtilisateurGroupe[] $utilisateurGroupesToPersist
     */
    private $utilisateurGroupesToPersist;
    
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    
    /**
     * @param \Twig_Environment $twig
     * @param RSAManager $rsa
     */
    public function __construct(ParameterBagInterface $params, SerializerInterface $serializer, UserPasswordEncoderInterface $passwordEncoder, UtilisateurGroupeRepository $utilisateurGroupeRepository, InfoPartageeGroupeRepository $infoPartageeGroupeRepository, EntityManagerInterface $em) {
        $this->params = $params;
        $this->serializer = $serializer;
        $this->passwordEncoder = $passwordEncoder;
        $this->utilisateurGroupeRepository = $utilisateurGroupeRepository;
        $this->infoPartageeGroupeRepository = $infoPartageeGroupeRepository;
        $this->cacheMotDePasseGroupe = [];
        $this->utilisateurGroupesToPersist = [];
        $this->em = $em;
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string $newPlainPassword
     * @param string $currentPrivatePassword
     * @return string
     */
    public function changePassword($utilisateur, $newPlainPassword, $currentPrivatePassword){
        $utilisateurGroupes = $utilisateur->getUtilisateurGroupes();
        
        //mise en cache des mots de passe des groupes
        foreach ($utilisateurGroupes as $utilisateurGroupe){
            $this->getPassword($utilisateurGroupe, $currentPrivatePassword);
        }
        
        // on affecte le nouveau mot de passe a l'utilisateur
        $utilisateur->setPassword(
            $this->passwordEncoder->encodePassword(
                    $utilisateur, $newPlainPassword
            )
        );
        
        //on en genere un nouveau privatePassword et on regenere les clef RSA de cet utilisateur
        $newPrivatePassword = $this->genererMotDePasseDerive($utilisateur, $newPlainPassword);
        $this->affecterClesRSA($utilisateur, $newPrivatePassword);
        
        //on chiffre les mots de passes des groupes avec le nouveau privatePassword
        // pas de confusion possible avec l'ancienne cle RSA car les mots de passes sont en cache
        foreach ($utilisateurGroupes as $utilisateurGroupe){
            $this->setPasswordGroupe($utilisateurGroupe, $utilisateur, $newPrivatePassword);
        }
        return $newPrivatePassword;
    }

    /**
     * @param string $activationCode
     * @param string $hashActivationCode
     */
    public function generateActivationCodeAndHash(&$activationCode, &$hashActivationCode, $tailleCode = 6) {
        /**
         * logA N = logB N / logB A
         * 
         * nb chiffre base 2 = log2(code)
         * nb chiffre base10 = log10(code) = log2 code / log2 10
         * 
         * nb chiffre base 2 = nb chiffre base10 * log2(code) / (log2 code / log2 10) 
         *                   = nb chiffre base10 * log2(code) * (log2 10 / log2 code) 
         *                   = nb chiffre base10 * log2 10
         *                   = nb chiffre base10 * ln 10 / ln 2
         */
        $nbChiffreBase2 = ceil($tailleCode * log(10) / log(2));
        $nbBytes = ceil($nbChiffreBase2 / 8);
        $activationCode = substr(hexdec(bin2hex(random_bytes($nbBytes))), 0, $tailleCode);
        $hashActivationCode = password_hash($activationCode, PASSWORD_ARGON2I);
    }

    /**
     * Generate a random string, using a cryptographically secure 
     * pseudorandom number generator (random_int)
     * 
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     * 
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    function randomStr(
    $length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= mb_substr($keyspace, random_int(0, $max), 1);
        }
        return $str;
    }

    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     */
    public function affecterClesRSA($utilisateur, $privatePassword) {
        $rsa = new RSA();
        $rsa->setPassword($privatePassword);
        //$rsa->setPrivateKeyFormat(RSA::PRIVATE_FORMAT_PKCS1);
        //$rsa->setPublicKeyFormat(RSA::PUBLIC_FORMAT_PKCS1);
        //define('CRYPT_RSA_EXPONENT', 65537);
        //define('CRYPT_RSA_SMALLEST_PRIME', 64); // makes it so multi-prime RSA is used
        $keys = $rsa->createKey($this->params->get('app.rsa_size')); // == $rsa->createKey(1024) where 1024 is the key size

        $utilisateur->setPrivateKey($keys['privatekey']);
        $utilisateur->setPublicKey($keys['publickey']);
    }

    /**
     * Dechiffre le mot de passe du groupe $groupe pour l'utilisateur $utilisateur
     * renvoie false si le mot de passe fournit est incorrect
     * renvoie false si l'utilisateur n'est pas le propriétaire du groupe
     * revoie false si l'utilisateur n'appartient pas au groupe
     * renvoie le mot de passe du groupe sinon
     * 
     * on notera que l'on ne pourra pas outrepasser les controles faits sur le mot de passe
     * car l'information voulue dépend de la valididé du mot de passe
     * un developpeur motivé à connaitre le mot de passe d'un groupe 
     * sans en faire partie ne pourra pas y parvenir avec seulement la base de données et le code source
     * 
     * il devra compromettre l'application pour intercepter ce mot de passe et attendra qu'une personne appartenant au groupe se connecte
     * 
     * @param Utilisateur $utilisateur
     * @param \App\Entity\Groupe $groupe
     * @param string $privatePassword
     * @return string|bool
     */
    public function getPasswordGroupe(Utilisateur $utilisateur, Groupe $groupe, $privatePassword) {
        if(
            isset($this->cacheMotDePasseGroupe[$utilisateur->getId()])
            &&  isset($this->cacheMotDePasseGroupe[$utilisateur->getId()][$groupe->getId()])
        ){
            //dump("using cache for user ".$utilisateur->getId()." and for group ".$groupe->getId());
            return $this->cacheMotDePasseGroupe[$utilisateur->getId()][$groupe->getId()];
        }
        $ug = $this->utilisateurGroupeRepository->findOneBy(['groupe' => $groupe, 'utilisateur' => $utilisateur]);
        
        if (!$ug) {
            return false;
        }
        
        return $this->getPassword($ug, $privatePassword);
    }
    
    /**
     * Persiste la liste des utilisateurGroupe à persister en base, après une mise en cache du mot de passe du groupe
     * Typiquement on chiffre le mot de passe du groupe avec la clef publique de l'utilisateur et l'utilisateur retrouve le mot de passe du groupe avec sa clef privée
     * 
     * Mais RSA demande beaucoup de ressources. Du coup, lorsque l'utilisateur retrouve le mot de passe du groupe, on mets en cache ce mot de passe dans utilisateurGroupesToPersist
     * 
     * Ce cache est chiffré avec AES avec le privatePassword, utilisant le même mot de passe que pour déchiffrer la clef privée du l'utilisateur
     * 
     */
    public function persistUtilisateurGroupesToPersist(){
        foreach ($this->utilisateurGroupesToPersist as $ug){
             $this->em->persist($ug);
        }
        $this->em->flush();
    }

        /**
     * @param UtilisateurGroupe $ug
     * @param string $privatePassword
     * @return string|bool
     */
    private function getPassword(UtilisateurGroupe $ug, $privatePassword){
        if(
            isset($this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()])
            &&  isset($this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()][$ug->getGroupe()->getId()])
        ){
            //dump("using cache for user ".$utilisateur->getId()." and for group ".$groupe->getId());
            return $this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()][$ug->getGroupe()->getId()];
        }
        $cipher = new AES(AES::MODE_CTR);
        if(!$cipher){
            return false;
        }
        // keys are null-padded to the closest valid size
        // longer than the longest key and it's truncated
        //$cipher->setKeyLength(256);
        $cipher->setKey($privatePassword);
        $motDePasseGroupe=false;
        if(!$ug->getPasswordCache()){
            if($ug->getPassword()){
                $encryptedPassword = base64_decode($ug->getPassword());

                $rsa = new RSA();
                $rsa->setPassword($privatePassword);
                $rsa->loadKey($ug->getUtilisateur()->getPrivateKey());
                $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);

                $motDePasseGroupe = $rsa->decrypt($encryptedPassword);
                if($motDePasseGroupe){
                     // the IV defaults to all-NULLs if not explicitly defined
                    $iv=Random::string($cipher->getBlockLength() >> 3);
                    $cipher->setIV($iv);

                    $ug->setPasswordCache(base64_encode($iv).'.'.base64_encode($cipher->encrypt($motDePasseGroupe)));
                    $this->utilisateurGroupesToPersist[] = $ug;
                }
            }
           
        }
        else{
            $tabPasswordCache = explode('.', $ug->getPasswordCache());
        
            $iv=base64_decode($tabPasswordCache[0]);
            $cipher->setIV($iv);
            $motDePasseGroupe = $cipher->decrypt(base64_decode($tabPasswordCache[1]));
        }
       
        if(!isset( $this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()])){
             $this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()] = [];
        }
        $this->cacheMotDePasseGroupe[$ug->getUtilisateur()->getId()][$ug->getGroupe()->getId()] = $motDePasseGroupe;
        //dump("creating cache for user ".$ug->getUtilisateur()->getId()." and for group ".$ug->getGroupe()->getId());
        return $motDePasseGroupe;
    }

    /**
     * Affecte le mot de passe du groupe pointe par $utilisateurGroupe à l'utilisateur pointe par $utilisateurGroupe
     * 
     * $utilisateurGroupe est l'entite a affecter
     * $utilisateurOrigine contient l'utlisateur à l'origine de l'affectation du mot de passe
     * $privatePassword contient le mot de passe de l'utilisateur courant pour pouvoir connaitre le mot de passe du groupe
     * 
     * on notera que l'on ne pourra pas outrepasser les controles faits sur le mot de passe
     * voir la doc de la fonction getPasswordGroupe pour savoir pourquoi
     * 
     * on notera également que l'on peut chiffrer une information qui ne sera lisible que par l'utilisateur cible grace 
     * a l'algorithme RSA qui permet de chiffrer avec un clef publique et de dechiffrer seulement avec la cle privee
     * 
     * @param UtilisateurGroupe $utilisateurGroupe
     * @param Utilisateur $utilisateurOrigine
     * @param string $privatePassword
     * @return bool
     */
    public function setPasswordGroupe(UtilisateurGroupe $utilisateurGroupe, Utilisateur $utilisateurOrigine, $privatePassword) {
        $utilisateurDestination = $utilisateurGroupe->getUtilisateur();
        $motDePasseGroupe = $this->getPasswordGroupe($utilisateurOrigine, $utilisateurGroupe->getGroupe(), $privatePassword);
        if (!$motDePasseGroupe) {
            return false;
        }
        if($utilisateurOrigine->getId() == $utilisateurDestination->getId()){
            
            $cipher = new AES(AES::MODE_CTR);
            if(!$cipher){
                return false;
            }
            // keys are null-padded to the closest valid size
            // longer than the longest key and it's truncated
            //$cipher->setKeyLength(256);
            $cipher->setKey($privatePassword);

            // the IV defaults to all-NULLs if not explicitly defined
            $iv=Random::string($cipher->getBlockLength() >> 3);
            $cipher->setIV($iv);

            $utilisateurGroupe->setPasswordCache(base64_encode($iv).'.'.base64_encode($cipher->encrypt($motDePasseGroupe)));
        }
        else{
            $rsa = new RSA();
            $rsa->loadKey($utilisateurDestination->getPublicKey());

            

            $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
            $encryptedPassword = $rsa->encrypt($motDePasseGroupe);

            $utilisateurGroupe->setPassword(base64_encode($encryptedPassword));
        }
        
        return true;
    }

    /**
     * Génère un nouveau mot de passe pour le groupe pointé par $utilisateurGroupe
     * Si le groupe pointé contient plusieurs autres utilisateurs, alors il y aura un incohérence entre le mot de passe généré et l'autre mot de passe existant pour ce groupe
     * Il ne faut donc pas utiliser cette fonction à partir du moment où elle a déjà été utilisée une fois sur le groupe
     * @param UtilisateurGroupe $utilisateurGroupe
     * @param string $privatePassword
     */
    public function setFirstPasswordGroupe(UtilisateurGroupe $utilisateurGroupe, $privatePassword) {
        //pas besoin de RSA, l'utilisateur qui chiffre le mot de passe du groupe est le propriétaire du groupe
        //on mets donc le mot de passe du groupe dirrectement dans le cache
        
        $utilisateurDestination = $utilisateurGroupe->getUtilisateur();       
        $motDePasseGroupe = $this->randomStr($this->params->get('app.passord_group_size'));
        
        $cipher = new AES(AES::MODE_CTR);
        if(!$cipher){
            return false;
        }
        // keys are null-padded to the closest valid size
        // longer than the longest key and it's truncated
        //$cipher->setKeyLength(256);
        $cipher->setKey($privatePassword);

        // the IV defaults to all-NULLs if not explicitly defined
        $iv=Random::string($cipher->getBlockLength() >> 3);
        $cipher->setIV($iv);

        $utilisateurGroupe->setPasswordCache(base64_encode($iv).'.'.base64_encode($cipher->encrypt($motDePasseGroupe)));
        
        
    }

    

    /**
     * @param string $existingHash
     * @param string $activationCodeToCheck
     * @return boolean
     */
    public function checkActivationCode($existingHash, $activationCodeToCheck) {
        return password_verify($activationCodeToCheck, $existingHash);
    }

    /**
     * @param Utilisateur $utilisateur
     */
    public function generateHashCodeExpiration(Utilisateur $utilisateur, &$activationCode, &$dateExpirationActivation) {
        $activationCode = "";
        $hashActivationCode = "";
        $this->generateActivationCodeAndHash($activationCode, $hashActivationCode);
        $utilisateur->setActivationCode($hashActivationCode);
        $dateExpirationActivation = new \DateTime("now");
        $temps = $this->params->get('limite_temps_activation_code');
        $dateExpirationActivation->modify('+' . $temps . 'sec');
        $utilisateur->setDateExpirationActivation($dateExpirationActivation);
    }
    
   
    

    /**
     * creer et renvoie le mot de passe derive du mot de passe principale pour pouvoir chiffrer le mot de passe du groupe
     * affecte l'utilisateur des informations necessaires à l'obtention du mot de passe derive 
     * 
     * @param Utilisateur $u
     * @param string $plainPassword
     * @return string
     */
    public function genererMotDePasseDerive(Utilisateur $utilisateur, $plainPassword){
        // le hash sera le mot de passe derive
        
        $salt = openssl_random_pseudo_bytes(16);
        $secretHash = hash_pbkdf2("sha256", $plainPassword, $salt, 1000, 20);        
        $utilisateur->setSaltDerivationPrivatePassword(base64_encode($salt));
        return $secretHash;
    }
    
    /**
     * obtient le mot de passe derive du mot de passe principale pour pouvoir dechiffrer le mot de passe du groupe
     * 
     * @param Utilisateur $u
     * @param string $plainPassword
     * @return string
     */
    public function getMotDePasseDerive(Utilisateur $utilisateur, $plainPassword){
        $salt = base64_decode($utilisateur->getSaltDerivationPrivatePassword());
        $secretHash = hash_pbkdf2("sha256", $plainPassword, $salt, 1000, 20); 
        return $secretHash;
    
    }

    
    

    /**
     * @param Utilisateur $utilisateur
     * @param type $privatePassword
     * @param \App\Entity\Groupe $groupe
     * @return AES|boolean
     */
    private function getCipher(Utilisateur $utilisateur, $privatePassword, Groupe $groupe){
        $motDePasseGroupe = $this->getPasswordGroupe($utilisateur, $groupe, $privatePassword);
        if (!$motDePasseGroupe) {
            return false;
        }
        $cipher = new AES(AES::MODE_CTR);
        // keys are null-padded to the closest valid size
        // longer than the longest key and it's truncated
        //$cipher->setKeyLength(256);
        $cipher->setKey($motDePasseGroupe);
        
        return $cipher;
    }
    
    /**
     * Dechiffre des info partagees en fonction du mot de passe du groupe des infos partagées
     * l'utilisateur $utilisateur doit appartenir au groupe sinon il sera techniquement impossible de connaitre le mot de passe du groupe : voir doc de la getPasswordGroupe
     * 
     * il est cette fois-ci necessaire de connaitre le mot de passe du groupe pour chiffrer une info car on utilise un chiffrement symétrique
     * 
     * @param \App\Entity\Utilisateur $utilisateur
     * @param \App\Entity\Groupe $groupe
     * @param string $privatePassword
     * @param string $infoPartageeChiffree
     * @return \App\DataClass\InfoPartageeClair|null
     */
    public function infoPartageeChiffreeToClair($utilisateur, $groupe, $privatePassword, $infoPartageeChiffree) {
        $cipher = $this->getCipher($utilisateur, $privatePassword, $groupe);
        if(!$cipher){
            return null;
        }
        $tabInfoPartageeChiffree = explode('.', $infoPartageeChiffree);
        
        $iv=base64_decode($tabInfoPartageeChiffree[0]);
        $cipher->setIV($iv);
        $data = $cipher->decrypt(base64_decode($tabInfoPartageeChiffree[1]));
        return $this->serializer->deserialize($data, InfoPartageeClair::class, 'json');
    }
    
    /**
     * Chiffre une info partagee en fonction du mot de passe du groupe de l'info partagée
     * l'utilisateur $utilisateur doit appartenir au groupe sinon il sera techniquement impossible de connaitre le mot de passe du groupe : voir doc de la getPasswordGroupe
     * 
     * il est cette fois-ci necessaire de connaitre le mot de passe du groupe pour chiffrer une info car on utilise un chiffrement symétrique
     * 
     * @param \App\Entity\Utilisateur $utilisateur
     * @param \App\Entity\Groupe $groupe
     * @param string $privatePassword
     * @param \App\DataClass\InfoPartageeClair $infoPartageeClair
     * @return null|string
     */
    public function infoPartageeClairToChiffree($utilisateur, $groupe, $privatePassword, $infoPartageeClair) {
        $cipher = $this->getCipher($utilisateur, $privatePassword, $groupe);
        if(!$cipher){
            return null;
        }
        // the IV defaults to all-NULLs if not explicitly defined
        $iv=Random::string($cipher->getBlockLength() >> 3);
        $cipher->setIV($iv);
        
        $data = $this->serializer->serialize($infoPartageeClair, 'json');
        return base64_encode($iv).'.'.base64_encode($cipher->encrypt($data));
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string $password
     */
    public function recuperationCompte(Utilisateur $utilisateur, $newPassword){
        $ipgsToDelete=[];
        $ugps = $this->utilisateurGroupeRepository->getUtilisateurGroupePerdusNonPerdus($utilisateur, true);//groupes perdus
        $ugrs = $this->utilisateurGroupeRepository->getUtilisateurGroupePerdusNonPerdus($utilisateur, false);//groupes recuperables
        
        $privatePassword = $this->genererMotDePasseDerive($utilisateur, $newPassword);
        $this->affecterClesRSA($utilisateur, $privatePassword);
        
        //recration des groupe non recuperables et suppression des infos partagees des groupes non recuperables
        foreach ($ugps as $ugp){
            $ugp->setPassword(null);
            $ugp->setPasswordCache(null);
            foreach ($ugp->getGroupe()->getInfoPartageeGroupes() as $infoPartageeGroupe){//toutes les infos partagees sont illisibles et doivent etre supprimées
                if(!isset($ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()])){
                    $ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()] = [];
                }
                $ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()][] = $infoPartageeGroupe;
            }
            $this->setFirstPasswordGroupe($ugp, $privatePassword);
        }
        
        //groupes recuperables
        //parmis les groupes recuperables, certains ont finalement acces aux infos partagées communes avec des groupes non recuperables
        //il faut donc annuler ces suppressions et permettre l'acces a ces info partagées par l'intermediaire des groupes recuperables
        foreach ($ugrs as $ugr){
            $ugr->setPassword(null);
            $ugr->setPasswordCache(null);
           
            //comme ce groupe est recuperables, les info partagees associes à [ce groupe et à des groupes non recuperables] sont recuperables
            foreach ($ugr->getGroupe()->getInfoPartageeGroupes() as $infoPartageeGroupe){
                if(isset($ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()])){//on ne supprime pas les InfoPartageeGroupes qui peuvent etre finalement accessibles
                    $ipgs = $ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()];
                    unset($ipgsToDelete[$infoPartageeGroupe->getInfoPartagee()->getId()]);
                    
                    foreach ($ipgs as $ipg){
                        $ipg->setInfoPartageeChiffree(null);//cette infoPartageeGroupe est assignée à un groupe non recuperable mais l'infoPartagée pointée est commune avec un autre infoPartageeGroupe qui est assigné à un groupe recuperable, donc ce $ipgToSave est finalement récupérable
                        $this->em->persist($ipg);
                    }
                }
            }
        }
        
        $utilisateur->setPassword(
                $this->passwordEncoder->encodePassword(
                        $utilisateur, $newPassword
                )
        );
        
        foreach ($ipgsToDelete as $ipgsToDelete2){
            foreach ($ipgsToDelete2 as $ipgToDelete){
                $this->em->remove($ipgToDelete);
            }
        }
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     * @return UtilisateurGroupe(]
     */
    public function restaurerUtilisateurGroupes(Utilisateur $utilisateur, $privatePassword){
        $ugs = $this
            ->utilisateurGroupeRepository
            ->getUtilisateurGroupesRecuperables($utilisateur)
            ;
        
        foreach ($ugs as $ug){
            if($ug->getUtilisateur()->getId() != $utilisateur->getId()){
                $this->setPasswordGroupe($ug, $utilisateur, $privatePassword);
                $this->em->persist($ug);
            }
        }
        $this->em->flush();
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     */
    public function restaurerInfoPartageeGroupe(Utilisateur $utilisateur, $privatePassword){
        $ipgs = $this
            ->infoPartageeGroupeRepository
            ->getAllInfoForUser($utilisateur, false)
            ;
        
        $ipgsOKParIP=[];
        $ipgsKOParIP=[];
        foreach ($ipgs as $ipg){
            $idInfoPartagee = $ipg->getInfoPartagee()->getId();
            if(!isset($ipgsOKParIP[$idInfoPartagee])){
                $ipgsOKParIP[$idInfoPartagee] = [];
                $ipgsKOParIP[$idInfoPartagee] = [];
            }
            if($ipg->getInfoPartageeChiffree() == null){
                $ipgsKOParIP[$idInfoPartagee][] = $ipg;
            }
            else{
                $ipgsOKParIP[$idInfoPartagee][] = $ipg;
            }
        }
        
        foreach ($ipgsOKParIP as $idInfoPartagee => $ipgsOKParIP2){
            
            if(isset($ipgsKOParIP[$idInfoPartagee]) && isset($ipgsOKParIP2[0])){
                $ipgsKOParIP2 = $ipgsKOParIP[$idInfoPartagee];
                $ipgOK = $ipgsOKParIP2[0];
                if($ipgOK->getGroupe() && (count($ipgOK->getGroupe()->getUtilisateurGroupes()) > 0)){
                  
                    $ug = $ipgOK->getGroupe()->getUtilisateurGroupes()[0];
                    $motDePasseGroupe = $this->getPassword($ug, $privatePassword);
                    if($motDePasseGroupe){  
                        $infoPartageeClair = $this->infoPartageeChiffreeToClair($utilisateur, $ipgOK->getGroupe(), $privatePassword, $ipgOK->getInfoPartageeChiffree()); 
                        if($infoPartageeClair){
                            foreach ($ipgsKOParIP2 as $ipgKO){
                                $infoPartageeChiffree = $this->infoPartageeClairToChiffree($utilisateur, $ipgKO->getGroupe(), $privatePassword, $infoPartageeClair);
                                $ipgKO->setInfoPartageeChiffree($infoPartageeChiffree);
                                $this->em->persist($ipgKO);
                            }
                        }
                    }
                }
                
                
            }
        }
        $this->em->flush();
    }



    public function invalidateCache(){
        $this->cacheMotDePasseGroupe = [];
    }
}
