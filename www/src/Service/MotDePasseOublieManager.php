<?php

namespace App\Service;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\UtilisateurRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\DataClass\AskCodeHash;

class MotDePasseOublieManager {
    
    /**
     * @var CryptoManager $cryptoManager 
     */
    private $cryptoManager;
    
    /**
     * @var SessionInterface $session
     */
    private $session;
    
    /**
     * @var ParameterBagInterface $params
     */
    private $params;
    
    /**
     * @var UtilisateurRepository $utilisateurRepository
     */
    private $utilisateurRepository;
    
   /**
    * @var MailManager $mailManager 
    */
    private $mailManager;
    
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder 
     */
    private $passwordEncoder;
        
    /**
     * @param \App\Service\EntityManagerInterface $em
     * @param \App\Service\MailManager $mailManager
     * @param \App\Service\CryptoManager $cryptoManager
     * @param SessionInterface $session
     * @param UtilisateurRepository $utilisateurRepository
     * @param ParameterBagInterface $params
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em, MailManager $mailManager, CryptoManager $cryptoManager, SessionInterface $session, UtilisateurRepository $utilisateurRepository, ParameterBagInterface $params) {
        $this->passwordEncoder = $passwordEncoder;
        $this->cryptoManager = $cryptoManager;
        $this->session = $session;
        $this->utilisateurRepository = $utilisateurRepository;
        $this->params = $params;
        $this->mailManager = $mailManager;
        $this->em = $em;        
    }
    
    
    public function init(){
        $this->session->set('recupUtilisateur', null);
        $this->session->set('recupCompteVerifOk', false);
    }

    /**
     * @param string $email
     */
    public function mailOK($email){//  
        $recupCompteCode="";
        $recupCompteHash="";
        $this->cryptoManager->generateActivationCodeAndHash($recupCompteCode, $recupCompteHash);
       
        $this->session->set('recupCompteHash', $recupCompteHash);

        $recupCompteExpiration = new \DateTime("now");
        $temps = $this->params->get('limite_temps_activation_code');
        $recupCompteExpiration->modify('+' . $temps . 'sec');
        $this->session->set('recupCompteExpiration', $recupCompteExpiration);

        $utilisateur = $this->utilisateurRepository->findOneBy(['email' => $email]);
        $this->session->set('recupUtilisateur', $utilisateur->getEmail());
        $this->mailManager->sendMailRecuperationCompte($utilisateur, $recupCompteCode, $recupCompteExpiration);
    }
    
    /**
     * @return (string|null)[]
     */
    public function checkAllowSaisieCode(){
        $this->session->set('recupCompteVerifOk', false);
        
        $recupCompteHash = $this->session->get('recupCompteHash', null);
        $recupCompteExpiration = $this->session->get('recupCompteExpiration', null);
        $askCodeHash = null;
        if($recupCompteHash && $recupCompteExpiration){
            $askCodeHash = (new AskCodeHash())->setHash($recupCompteHash)->setExpiration($recupCompteExpiration);
        }
        return [$recupCompteHash, $recupCompteExpiration, $askCodeHash];
    }
    
    public function saisieCodeOK(){
        $this->session->set('recupCompteHash', null);
        $this->session->set('recupCompteExpiration', null);
        $this->session->set('recupCompteVerifOk', true);
    }
    
    /**
     * @return (string|null)[]
     */
    public function checkAllowSaisieMdp(){
        $email = $this->session->get('recupUtilisateur', null);
        $utilisateur = $this->utilisateurRepository->findOneBy(['email' => $email]);
        $recupCompteVerifOk = $this->session->get('recupCompteVerifOk', false);
        return [$utilisateur, $recupCompteVerifOk];
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string|null $password
     */
    public function changerMdPAndPersist(Utilisateur $utilisateur, $password){
        if(
            ($password) // si un mot de passe est fournit 
            && (!$this->passwordEncoder->isPasswordValid($utilisateur, $password)) // si le mot de passe a changé
        ){
            $this->cryptoManager->recuperationCompte($utilisateur, $password); 
            $this->mailManager->sendMailChangementMotDePasse($utilisateur);
            $this->em->persist($utilisateur);
            $this->em->flush();                  
        }
    }

    public function saisieMdpOK(){
        $this->session->set('recupCompteVerifOk', false);
    }
    
    /**
     * @return Utilisateur
     */
    public function getUtilisateurMdpModifie(){
        $email = $this->session->get('recupUtilisateur', null);
        return $this->utilisateurRepository->findOneBy(['email' => $email]);
    }
}
