<?php

namespace App\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\Utilisateur;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GeneralManager {

    /**
     * renvoie la date $dateToFormat sous forme de chaine de caractere selon le format specifie dans $formatDate
     * toutes les lettres precedes de % seront remplacees par le format de la date correspondant (pour voir la liste des lettres disponible aller voir http://php.net/manual/fr/function.date.php dans la section format
     * 
     * exemple echo $generalManager->formaterDate(new \DateTime("now"), "%Y-%m-%d %H:%i:%s je peux mettre des Y des m  et des i sans qu'il ssoient remplaces !");
     * 
     * @param \DateTime $dateToFormat
     * @param string $formatDate
     * @return string
     */
    public function formaterDate(\DateTime $dateToFormat, $formatDate) {
        $pattern = '/[%](.)/';
        preg_match_all($pattern, $formatDate, $matches, PREG_PATTERN_ORDER);
        $strFormat = implode("-", $matches[1]);
        $strDateFormat = $dateToFormat->format($strFormat);
        $remplacements = explode("-", $strDateFormat);
        $cpt = 0;
        return preg_replace_callback(
                $pattern, function ($matches) use ($remplacements, &$cpt) {
            $retour = $remplacements[$cpt];
            $cpt++;
            return $retour;
        }, $formatDate
        );
    }

}
