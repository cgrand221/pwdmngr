<?php
namespace App\Service;

class MenuManager {
    
    /**
     *
     * @var string[]  $menu
     */
    private $menus = [
        [
            'icon' => 'fa-home',
            'libelle' => 'Accueil',
            'route' => 'accueil',
            'submenus' => [
            ]
        ],
        
        [
            'icon' => 'fa-group',
            'libelle' => 'Groupes',
            'keep_active_with_route' => [
                'groupe_edit' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier un groupe",
                ],
                
            ],
            'route' => 'groupe_index',
            'submenus' => [
                [
                    'route' => 'groupe_index',
                    'icon' => 'fa-list',
                    'libelle' => 'Mes groupes créés',
                ],
                [
                    'route' => 'groupe_membre_de_list',
                    'icon' => 'fa-user-times',
                    'libelle' => 'Mes autres groupes',
                ],
                [
                    'route' => 'groupe_new',
                    'icon' => 'fa-plus-circle',
                    'libelle' => 'Ajouter un groupe',
                ],
            ]
        ],
        [
            'icon' => 'fa-key',
            'libelle' => 'Mots de passe',
            'keep_active_with_route' => [
                'info_partagee_edit' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier un mot de passe",
                ],
                'info_partagee_show' =>  [
                    'icon' => 'fa-ye',
                    'libelle' => "Consulter un mot de passe",
                ],
                
            ],
            'route' => 'info_partagee_index',
            'submenus' => [
                [
                    'route' => 'info_partagee_index',
                    'icon' => 'fa-list ',
                    'libelle' => 'Liste',
                ],
                [
                    'route' => 'info_partagee_new',
                    'icon' => 'fa-plus-circle',
                    'libelle' => 'Ajouter un mot de passe',
                ],
            ]
        ],
        [
            'icon' => 'fa-user',
            'libelle' => 'Mon compte',
            'keep_active_with_route' => [
                'utilisateur_edit_forbidden' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier",
                ],
                'utilisateur_edit_allow' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier",
                ],
                'utilisateur_edit_email' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier l'email",
                ],
                'utilisateur_edit_email_allow_1' =>  [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier l'email - confirmer l'identité",
                ],
                'utilisateur_edit_email_allow_2' => [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier l'email - nouveau mail",
                ],
                'utilisateur_edit_email_allow_3' => [
                    'icon' => 'fa-edit',
                    'libelle' => "Modifier l'email - confirmer le nouveau mail",
                ],
            ],
            'route' => 'utilisateur_resume',
            'submenus' => [
                [
                    'route' => 'utilisateur_resume',
                    'icon' => 'fa-user',
                    'libelle' => 'Résumé',
                ],
                [
                    'route' => 'utilisateur_edit',
                    'icon' => 'fa-edit',
                    'libelle' => 'Modifier',
                ],
                
            ]
        ],
    ];
    
    /**
     * @param string $routeName
     */
    function addActiveClasses(&$routeName){
        
        foreach ($this->menus as $keyMenu => $menu) {
            foreach ($menu['submenus'] as $keySubmenu => $submenus) {
                $this->menus[$keyMenu]['submenus'][$keySubmenu]['active'] = false;
            }
            $this->menus[$keyMenu]['active'] = false;
        }

        foreach ($this->menus as $keyMenu => $menu) {
            $activeMenu=false;
            foreach ($menu['submenus'] as $keySubmenu => $submenus) {
                $activeMenu = (isset($submenus['route'])) && ($submenus['route'] == $routeName);
                if ($activeMenu) {
                    $this->menus[$keyMenu]['submenus'][$keySubmenu]['active'] = $activeMenu;
                    break;
                }
            }
            
            $activeMenu |=  (
                    isset($menu['keep_active_with_route']) 
                    && isset($menu['keep_active_with_route'][$routeName])
                );
            
            $activeMenu |= 
                (isset($menu['route'])) 
                && ($menu['route'] == $routeName)
                ;
            
            if ($activeMenu) {
                $this->menus[$keyMenu]['active'] = $activeMenu;
                break;
            }
        }
        
    }
    
    /**
     * @param string $routeName
     */
    public function getMenuAriane(&$routeName){
        $menu1=null;
        $menu2=null;
        foreach ($this->menus as $keyMenu => $menu) {
            $activeMenu =  (
                    isset($menu['keep_active_with_route']) 
                    && isset($menu['keep_active_with_route'][$routeName])
                );
            if($activeMenu){
                $menu2 = $menu['keep_active_with_route'][$routeName];
            }
            
            $activeMenu |= 
                (
                    isset($menu['route'])) 
                    && ($menu['route'] == $routeName
                )
                ;
            
            if (!$activeMenu) {
                foreach ($menu['submenus'] as $keySubmenu => $submenus) {
                    $activeMenu = (isset($submenus['route'])) && ($submenus['route'] == $routeName);
                    if ($activeMenu) {
                        $menu2 = $submenus;
                        break;
                    }
                }
            }
            if ($activeMenu) {
                $menu1 = $menu;
                break;
            }
        }
        $filAriane=[];
        if($menu1){
            $filAriane[] = $menu1;
        }
        if($menu2){
            $filAriane[] = $menu2;
        }
        return $filAriane;
    }
    
    public function getMenus(){
        return $this->menus;
    }
}
