<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ReCaptcha\ReCaptcha;

class RecaptchaManager {
    
    /**
     * @var ReCaptcha $reCaptcha
     */
    private $reCaptcha;
    
    /**
     * @var bool $isCaptchaActivated
     */
    private $isCaptchaActivated;

    /**
     * @param \ParameterBagInterface $params
     * @param \ReCaptcha $ReeCaptcha
     */
    public function __construct(ParameterBagInterface $params) {
        $this->reCaptcha = new ReCaptcha($params->get('app.recaptcha_secret_key'));
        $this->isCaptchaActivated = $params->get('app.use_recaptcha');
    }

    /**
     * @param string|null $token
     * @return boolean
     */
    public function checkToken($token){
        if(!$this->isCaptchaActivated){
            return true;
        }
        if(!$token){
            return false;
        }
        $response = $this->reCaptcha->verify($token);
        return ($response->isSuccess());
    }

}
