<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\CryptoManager;
use App\DataClass\AskCodeHash;
use App\DataClass\AskNewMail;
use App\Entity\Utilisateur;
use App\Repository\GroupeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UtilisateurManager {

    /**
     * @var SessionInterface $session 
     */
    private $session;

    /**
     * @var ParameterBagInterface $params
     */
    private $params;
    
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder
     */
    private $passwordEncoder;
    
    /**
     * @var CryptoManager $cryptoManager
     */
    private $cryptoManager;
    
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    
   /**
    * @var MailManager $mailManager 
    */
    private $mailManager;

    public function __construct(EntityManagerInterface $em, MailManager $mailManager, ParameterBagInterface $params, SessionInterface $session, UserPasswordEncoderInterface $passwordEncoder, CryptoManager $cryptoManager, GroupeRepository $groupeRepository) {
        $this->em = $em;  
        $this->mailManager = $mailManager;
        $this->session = $session;
        $this->params = $params;
        $this->passwordEncoder = $passwordEncoder;
        $this->cryptoManager = $cryptoManager;
        $this->groupeRepository = $groupeRepository;
    }

    public function init() {
        $this->session->set('utilisateurEditEmailHash1', null);
        $this->session->set('utilisateurEditEmailCodeExpiration1', null);
        $this->session->set('utilisateurEditEmailExpiration', null);
        $this->session->set('utilisateurEditEmailHash2', null);
        $this->session->set('utilisateurEditEmailCodeExpiration2', null);
        $this->session->set('newMail', null);
        $this->session->set('timeLimitUtilisateurEdit', null);
    }

    public function utilisateurEditSubmit() {
        $timeLimitUtilisateurEdit = new \DateTime("now");
        $temps = $this->params->get('limite_temps_utilisateur_edit');
        $timeLimitUtilisateurEdit->modify('+' . $temps . 'sec');
        $this->session->set('timeLimitUtilisateurEdit', $timeLimitUtilisateurEdit);
    }

    /**
     * @return bool
     */
    public function checkUtilisateurEditAllow() {
        $timeLimitUtilisateurEdit = $this->session->get('timeLimitUtilisateurEdit', null);
        return ($timeLimitUtilisateurEdit !== null) && ($timeLimitUtilisateurEdit >= new \DateTime("now"));
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string|null $plainPassword
     * @param string|null $privatePassword
     */
    public function utilisateurEditsubmitAndPersist(Utilisateur $utilisateur, $plainPassword, $privatePassword) {
        if (
            ($plainPassword) // si un mot de passe est fournit 
            && (!$this->passwordEncoder->isPasswordValid($utilisateur, $plainPassword)) // si le mot de passe a changé
        ) {
            $privatePassword = $this->cryptoManager->changePassword($utilisateur, $plainPassword, $privatePassword);
            $this->session->set('privatePassword', $privatePassword);
            $this->mailManager->sendMailChangementMotDePasse($utilisateur);
        }
        $this->em->persist($utilisateur);
        $this->em->flush();
    }

    /**
     * @param Utilisateur $utilisateur
     */
    public function utilisateurEditEmailSubmitAndPersist(Utilisateur $utilisateur){
        $activationCode1="";
        $utilisateurEditEmailHash1="";
        $this->cryptoManager->generateActivationCodeAndHash($activationCode1, $utilisateurEditEmailHash1);        
        $this->session->set('utilisateurEditEmailHash1', $utilisateurEditEmailHash1);
        $utilisateurEditEmailCodeExpiration1 = new \DateTime("now");
        $temps = $this->params->get('limite_temps_activation_code');
        $utilisateurEditEmailCodeExpiration1->modify('+' . $temps . 'sec');
        $this->session->set('utilisateurEditEmailCodeExpiration1', $utilisateurEditEmailCodeExpiration1);
        $this->mailManager->sendMailChangementEmail1($utilisateur, $activationCode1, $utilisateurEditEmailCodeExpiration1);
    }
    
    /**
     * @return AskCodeHash
     */
    public function prepareUtilisateurEditMailAllow1(){ 
        $utilisateurEditEmailHash1 = $this->session->get('utilisateurEditEmailHash1', null);
        $utilisateurEditEmailCodeExpiration1 = $this->session->get('utilisateurEditEmailCodeExpiration1', null);      
        $askCodeHash = new AskCodeHash();
        $askCodeHash->setHash($utilisateurEditEmailHash1);
        $askCodeHash->setExpiration($utilisateurEditEmailCodeExpiration1);
        return [$askCodeHash, $utilisateurEditEmailCodeExpiration1];
    }
    
    public function utilisateurEditMailAllow1Submit(){
        $this->session->set('utilisateurEditEmailHash1', null);
        $this->session->set('utilisateurEditEmailCodeExpiration1', null);
        $utilisateurEditEmailExpiration = new \DateTime("now");
        $temps = $this->params->get('limite_temps_utilisateur_edit');
        $utilisateurEditEmailExpiration->modify('+' . $temps . 'sec');
        $this->session->set('utilisateurEditEmailExpiration', $utilisateurEditEmailExpiration);
    }
    
    /**
     * @return AskNewMail
     */
    public function prepareUtilisateurEditMailAllow2() {
        $expiration = $this->session->get('utilisateurEditEmailExpiration', null);        
        $askNewMail = new AskNewMail();        
        $askNewMail->setExpiration($expiration);
        return $askNewMail;
    }
    
    /**
     * @param string $email
     */
    public function utilisateurEditMailAllow2Submit($email){
        $this->session->set('utilisateurEditEmailExpiration', null);
        $activationCode2="";
        $utilisateurEditEmailHash2="";
        $this->cryptoManager->generateActivationCodeAndHash($activationCode2, $utilisateurEditEmailHash2);      
        $this->session->set('utilisateurEditEmailHash2', $utilisateurEditEmailHash2);
        $utilisateurEditEmailCodeExpiration2 = new \DateTime("now");
        $temps = $this->params->get('limite_temps_activation_code');
        $utilisateurEditEmailCodeExpiration2->modify('+' . $temps . 'sec');
        $this->session->set('utilisateurEditEmailCodeExpiration2', $utilisateurEditEmailCodeExpiration2);
        $this->session->set('newMail', $email);
        $this->mailManager->sendMailChangementEmail2($email, $activationCode2, $utilisateurEditEmailCodeExpiration2);
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @return AskCodeHash
     */
    public function prepareUtilisateurEditMailAllow3(Utilisateur $utilisateur) {
        $utilisateurEditEmailHash2 = $this->session->get('utilisateurEditEmailHash2', null);
        $utilisateurEditEmailCodeExpiration2 = $this->session->get('utilisateurEditEmailCodeExpiration2', null);        
        $askCodeHash = new AskCodeHash();
        $askCodeHash->setHash($utilisateurEditEmailHash2);
        $askCodeHash->setExpiration($utilisateurEditEmailCodeExpiration2);
        return [$askCodeHash, $utilisateurEditEmailCodeExpiration2];
    }
    
    public function utilisateurEditMailAllow3SubmitAndPersist(utilisateur $utilisateur){
        $monGroupe = $this->groupeRepository->findOneBy(['nom' => $utilisateur->getEmail()]);
        $newMail = $this->session->get('newMail', $utilisateur->getEmail());
        $monGroupe->setNom($newMail);
        $utilisateur->setEmail($newMail);
        $this->em->persist($utilisateur);
        $this->em->flush();
        $this->session->set('utilisateurEditEmailHash2', null);
        $this->session->set('utilisateurEditEmailCodeExpiration2', null);
        $this->session->set('newMail', null);
    }
}
