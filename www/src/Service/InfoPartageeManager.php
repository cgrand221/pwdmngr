<?php

namespace App\Service;

use App\Entity\Utilisateur;
use App\Entity\Groupe;
use App\DataClass\InfoPartageeClair;
use App\Entity\InfoPartagee;
use App\Entity\InfoPartageeGroupe;
use App\Repository\InfoPartageeGroupeRepository;
use App\Repository\GroupeRepository;
use App\Repository\InfoPartageeRepository;
use Doctrine\ORM\EntityManagerInterface;


class InfoPartageeManager {

    /**
     * @var InfoPartageeGroupeRepository $infoPartageeGroupeRepository
     */
    private $infoPartageeGroupeRepository;

    /**
     * @var InfoPartageeRepository $infoPartageeRepository
     */
    private $infoPartageeRepository;

    /**
     * @var CryptoManager $cryptoManager 
     */
    private $cryptoManager;
    
    /**
     * @var EntityManagerInterface $em 
     */
    private $em;
    
    /**
     * @var GroupeRepository $groupeRepository 
     */
    private $groupeRepository;
    
    /**
     * @var Groupe $monGroupe
     */
    private $monGroupe;
    
    /**
     * @var InfoPartageeClair|null $infoPartageeClairOrigine
     */
    private $infoPartageeClairOrigine;
    
    /**
     * @var InfoPartageeGroupe[] $ipgs
     */
    private $ipgs;
    
    /**
     * @var int[] $ipgs
     */
    private $groupesByIdIPG;
    
    /**
     * @param InfoPartageeGroupeRepository $infoPartageeGroupeRepository
     * @param InfoPartageeRepository $infoPartageeRepository
     * @param GroupeRepository $groupeRepository
     * @param App\Service\CryptoManager $cryptoManager
     * @param EntityManagerInterface $em
     */
    public function __construct(InfoPartageeGroupeRepository $infoPartageeGroupeRepository, InfoPartageeRepository $infoPartageeRepository, GroupeRepository $groupeRepository, CryptoManager $cryptoManager, EntityManagerInterface $em) {
        $this->infoPartageeGroupeRepository = $infoPartageeGroupeRepository;
        $this->infoPartageeRepository = $infoPartageeRepository;
        $this->cryptoManager = $cryptoManager;
        $this->groupeRepository = $groupeRepository;
        $this->em = $em;
        $this->monGroupe = null;
        $this->infoPartageeClairOrigine = null;
        $this->ipgs=[];
        $this->groupesByIdIPG=[];
    }

    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     * @return array
     */
    public function getIndexInfos(Utilisateur $utilisateur, $privatePassword) {// manager
        $infoPartageeGroupesTmp = $this->infoPartageeGroupeRepository->getAllInfoForUser($utilisateur, true);
        $infoPartageeGroupes = [];
        foreach ($infoPartageeGroupesTmp as $infoPartageeGroupe) {
            $infoPartageeGroupes[$infoPartageeGroupe->getId()] = $infoPartageeGroupe;
        }
        $infoPartageeClairs = [];
        $infoPartagees = [];
        $ipAndIpgs = $this->infoPartageeRepository->getAllInfoForUser($utilisateur, false);

        foreach ($ipAndIpgs as $ipAndIpg) {
            $infoPartagees[] = $ipAndIpg['infoPartagee'];
            $ipg = $infoPartageeGroupes[$ipAndIpg['infoPartageeGroupe']];

            $infoPartageeChiffree = $ipg->getInfoPartageeChiffree();
            if ($infoPartageeChiffree !== null) {
                $infoPartageeClairs[] = $this->cryptoManager->infoPartageeChiffreeToClair($utilisateur, $ipg->getGroupe(), $privatePassword, $infoPartageeChiffree);
            } else
                $infoPartageeClairs[] = null;
        }

        $this->cryptoManager->persistUtilisateurGroupesToPersist();

        $ipAndIpgPbs = $this->infoPartageeRepository->getAllInfoForUser($utilisateur, true);
        $modificationPbs = [];
        foreach ($ipAndIpgPbs as $ipAndIpgPb) {
            $infoPartagee = $ipAndIpgPb['infoPartagee'];
            if ($infoPartagee->getProprietaire()->getId() == $utilisateur->getId()) {
                $modificationPbs[$infoPartagee->getId()] = true;
            }
        }
        return [$infoPartagees, $infoPartageeClairs, $modificationPbs];
    }

    /**
     * @param InfoPartagee $infoPartagee
     * @param Groupe $monGroupe
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     * @param string $infoPartageeClair
     */
    public function newInfoPartageeAndPersist(InfoPartagee $infoPartagee, Groupe $monGroupe, Utilisateur $utilisateur, $privatePassword, $infoPartageeClair) {// 
        $ipg = null;
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupes) {
            if ($monGroupe->getId() == $infoPartageeGroupes->getGroupe()->getId()) {
                $ipg = $infoPartageeGroupes;
                break;
            }
        }

        if (!$ipg) {
            $ipg = new InfoPartageeGroupe();
            $monGroupe->addInfoPartageeGroupe($ipg);
            $infoPartagee->addInfoPartageeGroupe($ipg);
        }
        $infoPartagee->setProprietaire($utilisateur);

        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupes) {
            $infoPartageeChiffree = $this->cryptoManager->infoPartageeClairToChiffree($utilisateur, $infoPartageeGroupes->getGroupe(), $privatePassword, $infoPartageeClair);
            $infoPartageeGroupes->setInfoPartageeChiffree($infoPartageeChiffree);
        }
        
        $this->em->persist($infoPartagee);
        $this->em->flush();
    }

    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     * @param InfoPartagee $infoPartagee
     * @return \App\DataClass\InfoPartageeClair|null
     */
    public function getShowInfo(Utilisateur $utilisateur, $privatePassword, InfoPartagee $infoPartagee){         
        $groupes = $this->groupeRepository->getMesGroupes($utilisateur, false, false, true);//groupes lisibles seulement
        $myGroups = [];
        foreach ($groupes as $groupe){
            if($groupe->getProprietaire()->getId() == $utilisateur->getId() || $groupe->getEtat() == Groupe::ETAT_ACTIF){
                 $myGroups[$groupe->getId()] = true;
            }
        }
        $ipg=null;
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupes){
            if(isset($myGroups[$infoPartageeGroupes->getGroupe()->getId()])){
                $ipg = $infoPartageeGroupes;
                break;
            }
        }
        if(!$ipg){// si je ne suis pas membre d'un des groupes affectes au mot de passe => pas d'autorisation de consultation
            return null;
        }
        $infoPartageeChiffree = $ipg->getInfoPartageeChiffree();
        return $this->cryptoManager->infoPartageeChiffreeToClair($utilisateur, $ipg->getGroupe(), $privatePassword, $infoPartageeChiffree);
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @param string $privatePassword
     * @param InfoPartagee $infoPartagee
     * @return InfoPartageeClair|null
     */
    public function prepareEdit(Utilisateur $utilisateur, $privatePassword, $infoPartagee){
        
        if($infoPartagee->getProprietaire()->getId() != $utilisateur->getId()){
            return null;
        }
        $groupes = $this->groupeRepository->getMesGroupes($utilisateur, false, false, true);//groupes actifs et inactifs
        $myGroups = [];
       
        foreach ($groupes as $groupe){
            $myGroups[$groupe->getId()] = true;
            if($groupe->getNom() == $utilisateur->getEmail()){
                $this->monGroupe = $groupe;
            }
        }
        if($this->monGroupe === null){//si je n'ai pas de groupe par defaut => pb
            return null;
        }
        
        
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupe){
            $this->ipgs[$infoPartageeGroupe->getGroupe()->getId()] = $infoPartageeGroupe;
            $this->groupesByIdIPG[$infoPartageeGroupe->getId()] = $infoPartageeGroupe->getGroupe()->getId();
        }
        
        if(!isset($this->ipgs[$this->monGroupe->getId()])){// si je ne suis pas membre des groupes affectes au mot de passe => pas d'autorisation de modification
            return null;
        }
        $membreDeTousLesGroupes=true;
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupe){
            if(!isset($myGroups[$infoPartageeGroupe->getGroupe()->getId()])){// si je ne suis pas memebre d'un des groupes affectes au mot de passe => pb
                $membreDeTousLesGroupes=false;
                break;
            }
        }
        if(!$membreDeTousLesGroupes){// si je ne suis pas membre des groupes affectes au mot de passe => pas d'autorisation de modification
            return null;
        }
        
        $infoPartageeChiffree = $this->ipgs[$this->monGroupe->getId()]->getInfoPartageeChiffree();
        $infoPartageeClair = $this->cryptoManager->infoPartageeChiffreeToClair($utilisateur, $this->monGroupe, $privatePassword, $infoPartageeChiffree);
        $this->infoPartageeClairOrigine = clone $infoPartageeClair;
        return $infoPartageeClair;
    }
    
    /**
     * @param InfoPartagee $infoPartagee
     * @param string $privatePassword
     * @param InfoPartageeClair $infoPartageeClair
     * @param Utilisateur $utilisateur
     */
    public function editAndPersist(InfoPartagee $infoPartagee, $privatePassword, InfoPartageeClair $infoPartageeClair, Utilisateur $utilisateur){
        $ipgs2=null;
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupe){
            if(isset($this->ipgs[$infoPartageeGroupe->getGroupe()->getId()]) && !$infoPartageeGroupe->getId()){//l'original a disparu et a ete remplace par la copie, or on prefere l'original a la copie
                $ipg = $this->ipgs[$infoPartageeGroupe->getGroupe()->getId()];
                $infoPartagee->removeInfoPartageeGroupe($infoPartageeGroupe);
                $ipg->getGroupe()->removeInfoPartageeGroupe($infoPartageeGroupe);
                $infoPartagee->addInfoPartageeGroupe($ipg);
                $ipg->getGroupe()->addInfoPartageeGroupe($ipg);
                $ipgs2[$infoPartageeGroupe->getGroupe()->getId()] = $ipg;

            }
            else{
                $ipgs2[$infoPartageeGroupe->getGroupe()->getId()] = $infoPartageeGroupe;
            }
        }

        if(!isset($ipgs2[$this->monGroupe->getId()])){// on remet en place l'original si il a disparu
            $infoPartagee->addInfoPartageeGroupe($this->ipgs[$this->monGroupe->getId()]);
            $this->monGroupe->addInfoPartageeGroupe($this->ipgs[$this->monGroupe->getId()]);
        }

        $infoPartageeClairModifiee = !$this->infoPartageeClairOrigine->equals($infoPartageeClair);
        
        foreach ($infoPartagee->getInfoPartageeGroupes() as $infoPartageeGroupe){
            if(
                $infoPartageeClairModifiee //info partagee changee
                || !$infoPartageeGroupe->getId() // nouveau groupe
                || $this->groupesByIdIPG[$infoPartageeGroupe->getId()] != $infoPartageeGroupe->getGroupe()->getId()//changement de groupe
            ){// on ne chiffre l'info que elle a ete modifie
                ;
                $infoPartageeChiffree = $this->cryptoManager->infoPartageeClairToChiffree($utilisateur, $infoPartageeGroupe->getGroupe(), $privatePassword, $infoPartageeClair);
                $infoPartageeGroupe->setInfoPartageeChiffree($infoPartageeChiffree);
            }

        }

        $this->em->persist($infoPartagee);
        $this->em->flush();
    }
}
