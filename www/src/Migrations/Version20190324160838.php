<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190324160838 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE utilisateur_groupe (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, groupe_id INT NOT NULL, password LONGTEXT DEFAULT NULL, password_cache LONGTEXT DEFAULT NULL, INDEX IDX_6514B6AAFB88E14F (utilisateur_id), INDEX IDX_6514B6AA7A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE info_partagee (id INT AUTO_INCREMENT NOT NULL, proprietaire_id INT NOT NULL, INDEX IDX_526EDFF176C50E4A (proprietaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE info_partagee_groupe (id INT AUTO_INCREMENT NOT NULL, groupe_id INT NOT NULL, info_partagee_id INT NOT NULL, info_partagee_chiffree LONGTEXT DEFAULT NULL, INDEX IDX_9BEDFA307A45358C (groupe_id), INDEX IDX_9BEDFA307EED490A (info_partagee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, proprietaire_id INT NOT NULL, nom VARCHAR(255) NOT NULL, etat INT NOT NULL, INDEX IDX_4B98C2176C50E4A (proprietaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, salt_derivation_private_password VARCHAR(255) NOT NULL, activation_code VARCHAR(255) NOT NULL, public_key LONGTEXT DEFAULT NULL, private_key LONGTEXT DEFAULT NULL, etat INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_expiration_activation DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1D1C63B3E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE utilisateur_groupe ADD CONSTRAINT FK_6514B6AAFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE utilisateur_groupe ADD CONSTRAINT FK_6514B6AA7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE info_partagee ADD CONSTRAINT FK_526EDFF176C50E4A FOREIGN KEY (proprietaire_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE info_partagee_groupe ADD CONSTRAINT FK_9BEDFA307A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE info_partagee_groupe ADD CONSTRAINT FK_9BEDFA307EED490A FOREIGN KEY (info_partagee_id) REFERENCES info_partagee (id)');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C2176C50E4A FOREIGN KEY (proprietaire_id) REFERENCES utilisateur (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE info_partagee_groupe DROP FOREIGN KEY FK_9BEDFA307EED490A');
        $this->addSql('ALTER TABLE utilisateur_groupe DROP FOREIGN KEY FK_6514B6AA7A45358C');
        $this->addSql('ALTER TABLE info_partagee_groupe DROP FOREIGN KEY FK_9BEDFA307A45358C');
        $this->addSql('ALTER TABLE utilisateur_groupe DROP FOREIGN KEY FK_6514B6AAFB88E14F');
        $this->addSql('ALTER TABLE info_partagee DROP FOREIGN KEY FK_526EDFF176C50E4A');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C2176C50E4A');
        $this->addSql('DROP TABLE utilisateur_groupe');
        $this->addSql('DROP TABLE info_partagee');
        $this->addSql('DROP TABLE info_partagee_groupe');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('DROP TABLE utilisateur');
    }
}
