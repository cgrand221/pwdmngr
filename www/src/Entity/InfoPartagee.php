<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoPartageeRepository")
 */
class InfoPartagee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="infoPartagees", cascade={"persist"}))
     * @ORM\JoinColumn(nullable=false)
     */
    private $proprietaire;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InfoPartageeGroupe", mappedBy="infoPartagee", orphanRemoval=true, cascade={"persist"})
     */
    private $infoPartageeGroupes;

    public function __construct()
    {
        $this->infoPartageeGroupes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    

   

    /**
     * @return Collection|InfoPartageeGroupe[]
     */
    public function getInfoPartageeGroupes(): Collection
    {
        return $this->infoPartageeGroupes;
    }

    public function addInfoPartageeGroupe(InfoPartageeGroupe $infoPartageeGroupe): self
    {
        if (!$this->infoPartageeGroupes->contains($infoPartageeGroupe)) {
            $allowAdd = true;
            foreach ($this->infoPartageeGroupes as $infoPartageeGroupes2){
                if($infoPartageeGroupes2->getGroupe() == $infoPartageeGroupe->getGroupe()){
                    $allowAdd=false;
                    break;
                }
            }
            if($allowAdd){
                $this->infoPartageeGroupes[] = $infoPartageeGroupe;
                $infoPartageeGroupe->setInfoPartagee($this);
            }
        }

        return $this;
    }

    public function removeInfoPartageeGroupe(InfoPartageeGroupe $infoPartageeGroupe): self
    {
        if ($this->infoPartageeGroupes->contains($infoPartageeGroupe)) {
            $this->infoPartageeGroupes->removeElement($infoPartageeGroupe);
            // set the owning side to null (unless already changed)
            if ($infoPartageeGroupe->getInfoPartagee() === $this) {
                $infoPartageeGroupe->setInfoPartagee(null);
            }
        }

        return $this;
    }

    public function getProprietaire(): ?Utilisateur
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Utilisateur $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }
}
