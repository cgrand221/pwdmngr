<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupeRepository")
 */
class Groupe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    
    const ETAT_ACTIF=1;
    const ETAT_INACTIF=2;
    /**
     * @ORM\Column(type="integer")
     */
    private $etat;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="groupesProprietaire", cascade={"persist"}))
     * @ORM\JoinColumn(nullable=false)
     */
    private $proprietaire;

    
    
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UtilisateurGroupe", mappedBy="groupe", orphanRemoval=true, cascade={"persist"})
     */
    private $utilisateurGroupes;

    

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InfoPartageeGroupe", mappedBy="groupe")
     */
    private $infoPartageeGroupes;

    public function __construct()
    {
        $this->utilisateurGroupes = new ArrayCollection();
        $this->infoPartageeGroupes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

   

    /**
     * @return Collection|UtilisateurGroupe[]
     */
    public function getUtilisateurGroupes(): Collection
    {
        return $this->utilisateurGroupes;
    }

    public function addUtilisateurGroupe(UtilisateurGroupe $utilisateurGroupe): self
    {
        if (!$this->utilisateurGroupes->contains($utilisateurGroupe)) {
            $allowAdd = true;
            foreach ($this->utilisateurGroupes as $utilisateurGroupe2){
                if($utilisateurGroupe2->getUtilisateur() == $utilisateurGroupe->getUtilisateur()){
                    $allowAdd=false;
                    break;
                }
            }
            if($allowAdd){
                $this->utilisateurGroupes[] = $utilisateurGroupe;
                $utilisateurGroupe->setGroupe($this);
            }
        }

        return $this;
    }

    public function removeUtilisateurGroupe(UtilisateurGroupe $utilisateurGroupe): self
    {
        if ($this->utilisateurGroupes->contains($utilisateurGroupe)) {
            $this->utilisateurGroupes->removeElement($utilisateurGroupe);
            // set the owning side to null (unless already changed)
            if ($utilisateurGroupe->getGroupe() === $this) {
                $utilisateurGroupe->setGroupe(null);
            }
        }

        return $this;
    }

    public function getProprietaire(): ?Utilisateur
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Utilisateur $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

    /**
     * @return Collection|InfoPartageeGroupe[]
     */
    public function getInfoPartageeGroupes(): Collection
    {
        return $this->infoPartageeGroupes;
    }

    public function addInfoPartageeGroupe(InfoPartageeGroupe $infoPartageeGroupe): self
    {
        if (!$this->infoPartageeGroupes->contains($infoPartageeGroupe)) {
            $allowAdd = true;
            foreach ($this->infoPartageeGroupes as $infoPartageeGroupes2){
                if($infoPartageeGroupes2->getGroupe() == $infoPartageeGroupe->getGroupe()){
                    $allowAdd=false;
                    break;
                }
            }
            if($allowAdd){
                $this->infoPartageeGroupes[] = $infoPartageeGroupe;
                $infoPartageeGroupe->setGroupe($this);
            }
           
        }

        return $this;
    }

    public function removeInfoPartageeGroupe(InfoPartageeGroupe $infoPartageeGroupe): self
    {
        if ($this->infoPartageeGroupes->contains($infoPartageeGroupe)) {
            $this->infoPartageeGroupes->removeElement($infoPartageeGroupe);
            
        }

        return $this;
    }
    
    
    
}
