<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoPartageeGroupeRepository")
 */
class InfoPartageeGroupe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groupe", inversedBy="infoPartageeGroupes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InfoPartagee", inversedBy="infoPartageeGroupes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $infoPartagee;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $infoPartageeChiffree;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getInfoPartagee(): ?InfoPartagee
    {
        return $this->infoPartagee;
    }

    public function setInfoPartagee(?InfoPartagee $infoPartagee): self
    {
        $this->infoPartagee = $infoPartagee;

        return $this;
    }

    public function getInfoPartageeChiffree(): ?string
    {
        return $this->infoPartageeChiffree;
    }

    public function setInfoPartageeChiffree(?string $infoPartageeChiffree): self
    {
        $this->infoPartageeChiffree = $infoPartageeChiffree;

        return $this;
    }

  
}
