<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UtilisateurRepository")
 * @UniqueEntity(fields={"email"}, message="Compte déjà existant")
 */
class Utilisateur implements UserInterface
{
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;
    
     /**
     * @ORM\Column(type="json")
     */
    private $roles = [];
    
    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;
    
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $saltDerivationPrivatePassword;
    
    /**
     * @var string The hashed activation code
     * @ORM\Column(type="string")
     */
    private $activationCode;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $publicKey;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $privateKey;



    
    const ETAT_EMAIL_NON_VERIFIE = 0;
    const ETAT_ACTIF = 1;
    /**
     * @ORM\Column(type="integer")
     */
    private $etat;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UtilisateurGroupe", mappedBy="utilisateur", orphanRemoval=true, cascade={"persist"})
     */
    private $utilisateurGroupes;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateExpirationActivation;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Groupe", mappedBy="proprietaire", orphanRemoval=true, cascade={"persist"})
     */
    private $groupesProprietaire;
    
    
/**
     * @ORM\OneToMany(targetEntity="App\Entity\InfoPartagee", mappedBy="proprietaire", orphanRemoval=true, cascade={"persist"})
     */
    private $infoPartagees;
   
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    /**
     * @return string 
     */
    public function getActivationCode(): string
    {
        return (string) $this->activationCode;
    }

    /**
     * @param string $activationCode 
     * @return $this
     */
    public function setActivationCode(string $activationCode): self
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    

    public function __construct()
    {
        $this->utilisateurGroupes = new ArrayCollection();
        $this->groupesProprietaire = new ArrayCollection();
        $this->infoPartagees = new ArrayCollection();
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|UtilisateurGroupe[]
     */
    public function getUtilisateurGroupes(): Collection
    {
        return $this->utilisateurGroupes;
    }

    public function addUtilisateurGroupe(UtilisateurGroupe $utilisateurGroupe): self
    {
        if (!$this->utilisateurGroupes->contains($utilisateurGroupe)) {
            $allowAdd = true;
            foreach ($this->utilisateurGroupes as $utilisateurGroupe2){
                if($utilisateurGroupe2->getGroupe() == $utilisateurGroupe->getGroupe()){
                    $allowAdd=false;
                    break;
                }
            }
            if($allowAdd){
                 $this->utilisateurGroupes[] = $utilisateurGroupe;
                $utilisateurGroupe->setUtilisateur($this);
            }
           
        }

        return $this;
    }

    public function removeUtilisateurGroupe(UtilisateurGroupe $utilisateurGroupe): self
    {
        if ($this->utilisateurGroupes->contains($utilisateurGroupe)) {
            $this->utilisateurGroupes->removeElement($utilisateurGroupe);
            // set the owning side to null (unless already changed)
            if ($utilisateurGroupe->getIdUtilisateur() === $this) {
                $utilisateurGroupe->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateExpirationActivation(): ?\DateTimeInterface
    {
        return $this->dateExpirationActivation;
    }

    public function setDateExpirationActivation(?\DateTimeInterface $dateExpirationActivation): self
    {
        $this->dateExpirationActivation = $dateExpirationActivation;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupesProprietaire(): Collection
    {
        return $this->groupesProprietaire;
    }

    public function addGroupesProprietaire(Groupe $groupesProprietaire): self
    {
        if (!$this->groupesProprietaire->contains($groupesProprietaire)) {
            $this->groupesProprietaire[] = $groupesProprietaire;
            $groupesProprietaire->setProprietaire($this);
        }

        return $this;
    }

    public function removeGroupesProprietaire(Groupe $groupesProprietaire): self
    {
        if ($this->groupesProprietaire->contains($groupesProprietaire)) {
            $this->groupesProprietaire->removeElement($groupesProprietaire);
            // set the owning side to null (unless already changed)
            if ($groupesProprietaire->getProprietaire() === $this) {
                $groupesProprietaire->setProprietaire(null);
            }
        }

        return $this;
    }

    
    public function getPublicKey(): ?string
    {
        return $this->publicKey;
    }

    public function setPublicKey(?string $publicKey): self
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function getPrivateKey(): ?string
    {
        return $this->privateKey;
    }

    public function setPrivateKey(?string $privateKey): self
    {
        $this->privateKey = $privateKey;

        return $this;
    }

    public function getSaltDerivationPrivatePassword(): ?string
    {
        return $this->saltDerivationPrivatePassword;
    }

    public function setSaltDerivationPrivatePassword(string $saltDerivationPrivatePassword): self
    {
        $this->saltDerivationPrivatePassword = $saltDerivationPrivatePassword;

        return $this;
    }

    /**
     * @return Collection|InfoPartagee[]
     */
    public function getInfoPartagees(): Collection
    {
        return $this->infoPartagees;
    }

    public function addInfoPartagee(InfoPartagee $infoPartagee): self
    {
        if (!$this->infoPartagees->contains($infoPartagee)) {
            $this->infoPartagees[] = $infoPartagee;
            $infoPartagee->setProprietaire($this);
        }

        return $this;
    }

    public function removeInfoPartagee(InfoPartagee $infoPartagee): self
    {
        if ($this->infoPartagees->contains($infoPartagee)) {
            $this->infoPartagees->removeElement($infoPartagee);
            // set the owning side to null (unless already changed)
            if ($infoPartagee->getProprietaire() === $this) {
                $infoPartagee->setProprietaire(null);
            }
        }

        return $this;
    }

}
