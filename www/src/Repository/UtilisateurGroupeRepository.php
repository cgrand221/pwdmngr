<?php

namespace App\Repository;

use App\Entity\UtilisateurGroupe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Utilisateur;
use App\Entity\Groupe;

/**
 * @method UtilisateurGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method UtilisateurGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method UtilisateurGroupe[]    findAll()
 * @method UtilisateurGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurGroupeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UtilisateurGroupe::class);
    }

     
    
    /**
     * @param Utilisateur $utilisateur
     * @param bool $perdus
     * @return UtilisateurGroupe[]
     */
    public function getUtilisateurGroupePerdusNonPerdus(Utilisateur $utilisateur, $perdus){
        $groupesAvecAuMoinsUnAutreUtilisateur = $this->createQueryBuilder('ug1');
        
        $groupesAvecAuMoinsUnAutreUtilisateur
            ->select('max(g1.id)')
            ->innerJoin('ug1.groupe', 'g1')
            ->innerJoin(Groupe::class, 'g2', 'with', 'g2.id = g1.id')
            ->innerJoin('g2.utilisateurGroupes', 'ug2')
            ->innerJoin('ug2.utilisateur', 'u2')
            ->innerJoin('ug1.utilisateur', 'u')
            ->andWhere('u = :utilisateur')
            ->andWhere('u2 <> :utilisateur')
            ->groupBy('g1.id')
            ;
       
        
        $query = $this->createQueryBuilder('ug3');
       
        $query
            ->select('ug3')
            ->addSelect('g3')
            ->addSelect('ipg')
            ->innerJoin('ug3.groupe','g3')
            ->innerJoin('g3.infoPartageeGroupes','ipg')
            ->innerJoin('ug3.utilisateur','u3')
                ;
        
        if($perdus){
            $query
                ->where($query->expr()->notIn('g3.id', $groupesAvecAuMoinsUnAutreUtilisateur->getDQL()))
                ;
        }
        else{
            $query
                ->where($query->expr()->in('g3.id', $groupesAvecAuMoinsUnAutreUtilisateur->getDQL()))
                ;
        }
        
        return $query
            ->andWhere('u3 = :utilisateur')
            ->setParameter('utilisateur', $utilisateur)
            ->getQuery()
            ->getResult()
                ;
        
            
    }
    
    /**
     * @param Utilisateur $utilisateur
     * @return UtilisateurGroupe[]
     */
    public function getUtilisateurGroupesRecuperables(Utilisateur $utilisateur){// quels sont les utilisateurGroupes qui peuvent etre aides par mes groupes dont je connais le mot de passe ?
        return $this
            ->createQueryBuilder('ug1')
            ->select('ug1')
            ->addSelect('g1')
            ->innerJoin('ug1.groupe','g1')
            ->innerJoin('ug1.utilisateur','u1')
            ->innerJoin(Groupe::class, 'g2', 'with', 'g2.id = g1.id')
            ->innerJoin('g2.utilisateurGroupes', 'ug2')
            ->innerJoin('ug2.utilisateur', 'u2')
            
                
            ->andWhere('u2 = :utilisateur')
            ->andWhere('ug2.password is not null or ug2.passwordCache is not null')
            ->andWhere('ug1.password is null and ug1.passwordCache is null')
            ->setParameter('utilisateur', $utilisateur)
            ->getQuery()
            ->getResult()
                ;
    }
   
}
