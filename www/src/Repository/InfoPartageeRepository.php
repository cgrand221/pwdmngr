<?php

namespace App\Repository;

use App\Entity\InfoPartagee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Utilisateur;
use App\Entity\Groupe;
/**
 * @method InfoPartagee|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoPartagee|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoPartagee[]    findAll()
 * @method InfoPartagee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoPartageeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoPartagee::class);
    }
    
    /**
     * Permet d'obtenir la liste des info partagées disponibles pour $utilisateur
     * Si $onlyWithEmptyUtilisateurGroupesPassword est à true, on obtient seulement la liste des info partagées disponibles dont certains groupes sont illisibles
     * 
     * @param Utilisateur $utilisateur
     * @return InfoPartagee[]
     */
    public function getAllInfoForUser($utilisateur, $onlyWithEmptyUtilisateurGroupesPassword=false){
        $query = $this->createQueryBuilder('i')
                ->select('i as infoPartagee')
                ->addSelect('min(ipg.id) as infoPartageeGroupe')
                ->innerJoin('i.infoPartageeGroupes', 'ipg')
                ->innerJoin('ipg.groupe', 'g')
                ->innerJoin('g.utilisateurGroupes', 'ug')
                ->innerJoin('ug.utilisateur', 'u')
                ->andWhere('u = :utilisateur')
                ->andWhere('(g.etat = :etat or g.proprietaire = :utilisateur)')
                ->setParameter('utilisateur', $utilisateur)
                ->setParameter('etat', Groupe::ETAT_ACTIF)
                ;
        if($onlyWithEmptyUtilisateurGroupesPassword){
            $query
                ->andWhere('ug.password is null')
                ->andWhere('ug.passwordCache is null')
                ;
        }
        return $query
                ->groupBy('i.id')
                ->getQuery()
                ->getResult()
                ;
    }
    
    
    

    // /**
    //  * @return InfoPartagee[] Returns an array of InfoPartagee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InfoPartagee
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
