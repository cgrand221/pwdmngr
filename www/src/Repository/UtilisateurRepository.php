<?php

namespace App\Repository;

use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Utilisateur::class);
    }

    // ['email' => $activationCompte->getEmail(), 'etat' => Utilisateur::ETAT_EMAIL_NON_VERIFIE]
    /**
     * 
     * @param string $email
     * @return Utilisateur|null
     */
    public function findForActivation($email) {
        
        return $this->createQueryBuilder('u')
                        ->andWhere('u.email = :email')
                        ->setParameter('email', $email)
                        ->andWhere('u.etat = :etat')
                        ->setParameter('etat', Utilisateur::ETAT_EMAIL_NON_VERIFIE)
                        ->andWhere('u.dateExpirationActivation > :dateExpirationActivation')
                        ->setParameter('dateExpirationActivation', new \DateTime("now"))
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

    /*
      public function findOneBySomeField($value): ?Utilisateur
      {
      return $this->createQueryBuilder('u')
      ->andWhere('u.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
