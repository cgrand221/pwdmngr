<?php

namespace App\Repository;

use App\Entity\Groupe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Utilisateur;

/**
 * @method Groupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Groupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Groupe[]    findAll()
 * @method Groupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Groupe::class);
    }
    
    /**
     * renvoie tous les groupes dont utilisateurGroupe est associe à $utilisateur 
     * associe une ligne de utilisateurGroupe au resultat, qui l'utilisateurGroupe correspondant à $utilisateur
     * 
     * $exclureProprietaire permet d'exclure les groupes dont $utilisateur est propriétaire
     * $seulementActifs permet d'exclure les groupes non  actifs
     *  
     * @param Utilisateur $utilisateur
     * @param bool $exclureProprietaire
     * @param bool $seulementActifs
     * @param Groupe|null $forceOneGroupe
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getMesGroupesQB(Utilisateur $utilisateur, $exclureProprietaire=false, $seulementActifs=false, $seulementLisibles=false, $forceOneGroupe=null){
        $queryBuilder= $this->createQueryBuilder('g')
            ->select('g')
            ->addSelect('ug')
            ->innerJoin('g.utilisateurGroupes', 'ug')
            ->innerJoin('ug.utilisateur', 'u')
            ->andWhere('u = :utilisateur')
                ;
        if($exclureProprietaire){
            $queryBuilder
                ->andWhere('g.proprietaire <> :utilisateur')
                    ;
        }
        if($seulementLisibles){
             $queryBuilder
                ->andWhere('ug.password is not null or ug.passwordCache is not null')
                    ;
        }
        if($forceOneGroupe){
            $queryBuilder
                ->andWhere('g = :forceOneGroupe')
                ->setParameter('forceOneGroupe', $forceOneGroupe)
            ;
        }
        $queryBuilder->setParameter('utilisateur', $utilisateur)
                ;
        
        if($seulementActifs){
            $queryBuilder
                ->andWhere('g.etat = :etat')
                ->setParameter('etat', Groupe::ETAT_ACTIF)
                    ;
        }     
        return $queryBuilder
            ->orderBy('g.id', 'ASC');
    }
    /**
     * @param Utilisateur $utilisateur
     * @param bool $exclureProprietaire
     * @param bool $seulementActifs
     * @return Groupe(]
     */
    public function getMesGroupes(Utilisateur $utilisateur, $exclureProprietaire=false, $seulementActifs=false, $seulementLisibles=false){
        return $this->getMesGroupesQB($utilisateur, $exclureProprietaire, $seulementActifs, $seulementLisibles)
            ->getQuery()
            ->getResult();
    }
    
    /**
     * renvoie les groupes dont le propriétaire est $utilisateur
     * associe une ligne de utilisateurGroupe au resultat, qui l'utilisateurGroupe correspondant à $utilisateur
     * 
     * @param Utilisateur $utilisateur
     * @param bool $seulementActifs
     * @return Groupe(]
     */
    public function getMesGroupesCrees(Utilisateur $utilisateur, $seulementActifs=false){
        $queryBuilder= $this->createQueryBuilder('g')
            ->select('g')
            ->addSelect('ug')
            ->innerJoin('g.utilisateurGroupes', 'ug')
            ->innerJoin('ug.utilisateur', 'u')
            ->andWhere('u = :utilisateur')
            ->andWhere('g.proprietaire = :utilisateur')
            ->setParameter('utilisateur', $utilisateur)
                ;
        
        if($seulementActifs){exit;
            $queryBuilder
                ->andWhere('g.etat = :etat')
                ->setParameter('etat', Groupe::ETAT_ACTIF)
                    ;
        }     
        return $queryBuilder
            ->orderBy('g.id', 'ASC') 
            ->getQuery()
            ->getResult();
    }

    
}
