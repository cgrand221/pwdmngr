<?php

namespace App\Repository;

use App\Entity\InfoPartageeGroupe;
use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Groupe;
use App\Entity\UtilisateurGroupe;

/**
 * @method InfoPartageeGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoPartageeGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoPartageeGroupe[]    findAll()
 * @method InfoPartageeGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoPartageeGroupeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoPartageeGroupe::class);
    }

    /**
     * @param Utilisateur $utilisateur
     * @param bool $onlyGroupeActif
     * @return InfoPartageeGroupe[]
     */
    public function getAllInfoForUser(Utilisateur $utilisateur, $onlyGroupeActif){
        $query = $this->createQueryBuilder('ipg')
                ->addSelect('g')
                ->addSelect('ug')
                ->innerJoin('ipg.groupe', 'g')
                ->innerJoin('g.utilisateurGroupes', 'ug')
                ->innerJoin('ug.utilisateur', 'u')
                ->andWhere('u = :utilisateur')
                ->setParameter('utilisateur', $utilisateur)
                ;
        if($onlyGroupeActif){
            $query
                ->andWhere('(g.etat = :etat or g.proprietaire = :utilisateur)')
                ->setParameter('etat', Groupe::ETAT_ACTIF)
                    ;
        }
                
        return $query
                ->getQuery()
                ->getResult()
                ;
    }
    
   /**
     * @param UtilisateurGroupe[] $utilisateurGroupe
     * @return InfoPartageeGroupe[]
     */
    public function getInfosNonMembre($utilisateurGroupe){
        return $this->createQueryBuilder('ipg')
            ->select('ipg')
            ->innerJoin(UtilisateurGroupe::class, 'ug', 'with', 'ug.groupe = ipg.groupe')
            ->innerJoin('ipg.infoPartagee', 'ip')
            ->andWhere('ip.proprietaire = ug.utilisateur')
            ->andWhere('ug in (:utilisateurGroupe)')
            ->setParameter('utilisateurGroupe', $utilisateurGroupe)
            ->getQuery()
            ->getResult()
                ;
    }
    
    
}
