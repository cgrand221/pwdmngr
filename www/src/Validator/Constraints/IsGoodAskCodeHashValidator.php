<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\DataClass\AskCodeHash;

class IsGoodAskCodeHashValidator extends ConstraintValidator {
    

    public function validate($askCodeHash, Constraint $constraint) {
        if (!$constraint instanceof IsGoodAskCodeHash) {
            throw new UnexpectedTypeException($constraint, IsGoodAskCodeHash::class);
        }
       
        if(!$askCodeHash instanceof AskCodeHash){
            throw new UnexpectedTypeException($askCodeHash, AskCodeHash::class);
        }
        
        
        if(!password_verify($askCodeHash->getCode(), $askCodeHash->getHash())){
             $this->context->buildViolation($constraint->message_code)
                ->addViolation();
        }
        
    }

}
