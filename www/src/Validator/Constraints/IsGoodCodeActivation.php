<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsGoodCodeActivation extends Constraint
{
    public $message = "Code non valide";
    
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}