<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\DataClass\UtilisateurPasswordCheck;

class IsGoodPasswordCheckValidator extends ConstraintValidator {
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder 
     */
    private $passwordEncoder;
    
    
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function validate($utilisateurPasswordCheck, Constraint $constraint) {
        if (!$constraint instanceof IsGoodPasswordCheck) {
            throw new UnexpectedTypeException($constraint, IsGoodPasswordCheck::class);
        }
       
        if(!$utilisateurPasswordCheck instanceof UtilisateurPasswordCheck){
            throw new UnexpectedTypeException($utilisateurPasswordCheck, UtilisateurPasswordCheck::class);
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $utilisateurPasswordCheck) {
            throw new UnexpectedTypeException($utilisateurPasswordCheck, UtilisateurPasswordCheck::class);
        }
        
        if(!$this->passwordEncoder->isPasswordValid($utilisateurPasswordCheck->getUtilisateur(), $utilisateurPasswordCheck->getPassword())){
             $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
