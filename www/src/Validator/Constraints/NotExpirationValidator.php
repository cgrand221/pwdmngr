<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotExpirationValidator extends ConstraintValidator {
    
    public function validate($date, Constraint $constraint) {
        if (!$constraint instanceof NotExpiration) {
            throw new UnexpectedTypeException($constraint, NotExpiration::class);
        }
        
        if($date === null){
            return;
        }
        
        if(!$date instanceof \DateTime){
            throw new UnexpectedTypeException($token, \String::class);
        }
        
        if(new \DateTime("now") > $date){
             $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
       
    }

}
