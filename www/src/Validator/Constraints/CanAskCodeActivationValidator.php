<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Repository\UtilisateurRepository;
use App\DataClass\AskNewCode;
use App\Entity\Utilisateur;

class CanAskCodeActivationValidator extends ConstraintValidator {
    
    
    /**
     * @var UtilisateurRepository $utilisateurRepository 
     */
    private $utilisateurRepository;
    
    public function __construct(UtilisateurRepository $utilisateurRepository) {
        $this->utilisateurRepository = $utilisateurRepository;
    }

    public function validate($askNewCode, Constraint $constraint) {
        if (!$constraint instanceof CanAskCodeActivation) {
            throw new UnexpectedTypeException($constraint, CanAskCodeActivation::class);
        }
       
        if(!$askNewCode instanceof AskNewCode){
            throw new UnexpectedTypeException($askNewCode, AskNewCode::class);
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $askNewCode) {
            return;
        }
        
        $utilisateur = $this->utilisateurRepository->findOneBy(["email" => $askNewCode->getEmail()]);
        if($utilisateur == null){
             $this->context->buildViolation($constraint->messageMailInexistant)
                ->addViolation();
        }
        else if($utilisateur->getEtat() == Utilisateur::ETAT_ACTIF){
             $this->context->buildViolation($constraint->messageMailDejaActif)
                ->addViolation();
        }
    }

}
