<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Service\RecaptchaManager;

class IsRecaptchaValidValidator extends ConstraintValidator {
    
    /**
     * @var RecaptchaManager $recaptchaManager
     */
    private $recaptchaManager;


    /**
     * @param RecaptchaManager $RecaptchaManager
     */
    public function __construct(RecaptchaManager $recaptchaManager) {
        $this->recaptchaManager = $recaptchaManager;
    }

    public function validate($token, Constraint $constraint) {
        if (!$constraint instanceof IsRecaptchaValid) {
            throw new UnexpectedTypeException($constraint, IsRecaptchaValid::class);
        }
        
        if($token && !is_string($token)){
            throw new UnexpectedTypeException($token, \String::class);
        }
        
        if(!$this->recaptchaManager->checkToken($token)){
             $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
       
    }

}
