<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsGoodPasswordCheck extends Constraint
{
    public $message = "Mot de passe incorrect";
    
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}