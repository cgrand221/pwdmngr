<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsGoodAskCodeHash extends Constraint
{
    public $message_code = "Code non valide";
    
    
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}