<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsNotEmail extends Constraint
{
    public $message = "Le nom d'un groupe ne doit pas contenir de caractères spéciaux";
    
    
}