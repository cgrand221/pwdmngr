<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Repository\UtilisateurRepository;
use App\Validator\Constraints\IsGoodNewMail;

class IsGoodNewMailValidator extends ConstraintValidator {
    
    
    /**
     * @var UtilisateurRepository $utilisateurRepository 
     */
    private $utilisateurRepository;
    
    public function __construct(UtilisateurRepository $utilisateurRepository) {
        $this->utilisateurRepository = $utilisateurRepository;
    }

    public function validate($email, Constraint $constraint) {
        if (!$constraint instanceof IsGoodNewMail) {
            throw new UnexpectedTypeException($constraint, IsGoodNewMail::class);
        }
       
        if(!is_string($email)){
            throw new UnexpectedTypeException($email, "string");
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $email) {
            return;
        }
        
        $utilisateur = $this->utilisateurRepository->findOneBy(["email" => $email]);
        if(
            $utilisateur 
        ){
             $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
