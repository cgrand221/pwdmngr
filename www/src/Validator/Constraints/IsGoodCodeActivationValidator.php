<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Service\CryptoManager;
use App\Repository\UtilisateurRepository;
use App\DataClass\ActivationCompte;
use App\Entity\Utilisateur;

class IsGoodCodeActivationValidator extends ConstraintValidator {
    /**
     * @var CryptoManager $cryptoManager 
     */
    private $cryptoManager;
    
    /**
     * @var UtilisateurRepository $utilisateurRepository 
     */
    private $utilisateurRepository;
    
    public function __construct(CryptoManager $cryptoManager, UtilisateurRepository $utilisateurRepository) {
        $this->cryptoManager = $cryptoManager;
        $this->utilisateurRepository = $utilisateurRepository;
    }

    public function validate($activationCompte, Constraint $constraint) {
        if (!$constraint instanceof IsGoodCodeActivation) {
            throw new UnexpectedTypeException($constraint, IsGoodCodeActivation::class);
        }
       
        if(!$activationCompte instanceof ActivationCompte){
            throw new UnexpectedTypeException($activationCompte, ActivationCompte::class);
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $activationCompte) {
            return;
        }
        
        $utilisateur = $this->utilisateurRepository->findOneBy(["email" => $activationCompte->getEmail(), "etat" => Utilisateur::ETAT_EMAIL_NON_VERIFIE]);
        if(
                !$utilisateur 
                || !$this->cryptoManager->checkActivationCode($utilisateur->getActivationCode(), $activationCompte->getCode())
                ){
             $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}
