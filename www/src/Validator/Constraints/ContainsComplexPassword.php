<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsComplexPassword extends Constraint
{
    public $message = "Ce mot de passe n'est pas assez compliqué. Il doit contenir au moins 2 minuscules, 2 majuscules, 2 chiffres et 2 caractères spéciaux";
}