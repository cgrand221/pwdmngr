<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CanAskCodeActivation extends Constraint
{
    public $messageMailInexistant = "Mail inexistant";
    public $messageMailDejaActif = "Mail déjà activé";
    
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}