<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ContainsComplexPasswordValidator extends ConstraintValidator {

    public function validate($value, Constraint $constraint) {
        if (!$constraint instanceof ContainsComplexPassword) {
            throw new UnexpectedTypeException($constraint, ContainsComplexPassword::class);
        }
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }
        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }
        preg_match_all('/[A-Z]/', $value, $upperCaseMatches, PREG_PATTERN_ORDER);
        preg_match_all('/[a-z]/', $value, $lowerCaseMatches, PREG_PATTERN_ORDER);
        preg_match_all('/[0-9]/', $value, $digitMatches, PREG_PATTERN_ORDER);

        $nbUpperCase = count($upperCaseMatches[0]);
        $nbLowerCase = count($lowerCaseMatches[0]);
        $nbDigit = count($digitMatches[0]);
        $nbSpecialChars = mb_strlen($value) - $nbUpperCase - $nbLowerCase - $nbDigit;

        if(
                ($nbUpperCase < 2)
                || ($nbLowerCase < 2)
                || ($nbDigit < 2)
                || ($nbSpecialChars < 2)
        ){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
        
    }

}
