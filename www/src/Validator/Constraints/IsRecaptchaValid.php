<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsRecaptchaValid extends Constraint
{
    public $message = "captcha non valide";
    
    
}