<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;


class IsNotEmailValidator extends ConstraintValidator {
    
    public function validate($value, Constraint $constraint) {
        if (!$constraint instanceof IsNotEmail) {
            throw new UnexpectedTypeException($constraint, IsNotEmail::class);
        }
       
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value) {
            return;
        }
        
        if(!is_string($value)){
            throw new UnexpectedTypeException($value, \String::class);
        }
         preg_match_all('/[^A-Za-z0-9 ]/', $value, $matches, PREG_PATTERN_ORDER);
        if (count($matches[0]) > 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
        
    }

}
