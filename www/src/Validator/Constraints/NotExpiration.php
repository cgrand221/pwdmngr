<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotExpiration extends Constraint
{
    public $message = "Le formulaire a expiré";
    
    
  
}