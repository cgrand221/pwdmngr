<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsGoodExistingMail extends Constraint
{
    public $message = "Email inexistant";
    
    
    
}