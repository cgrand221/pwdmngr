<?php

namespace App\Form;
use Symfony\Component\Form\AbstractType;
use App\DataClass\UtilisateurPasswordCheck;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Validator\Constraints\IsRecaptchaValid;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UtilisateurPasswordCheckType  extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('password', PasswordType::class, [
                'label' => "Mot de passe",
            ])
            ->add('recaptchaToken', HiddenType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'recaptcha_token',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => UtilisateurPasswordCheck::class,
        ]);
    }
}
