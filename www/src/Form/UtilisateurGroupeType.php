<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\UtilisateurGroupe;
use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UtilisateurGroupeType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event)  {
            $ug = $event->getData();
            $form = $event->getForm();

            // checks if the UtilisateurGroupe object is "new"
            // If no data is passed to the form, the data is "null".
            // This should be considered a existing "UtilisateurGroupe"
            if ($ug && null !== $ug->getId()) {
                $form->add('utilisateur', EntityType::class, [
                    "class" => Utilisateur::class,
                    "choice_label" => "email",
                    "attr" => [
                        "class" => "select2",
                    ],
                    "label" => false,
                    "query_builder" => function (UtilisateurRepository $utilisateurRepository) use ($ug) {
                        return $utilisateurRepository->createQueryBuilder('u')->where('u = :utilisateur')->setParameter('utilisateur', $ug->getUtilisateur());
                    },
                ])
                ;
            }
            else{
                 $form->add('utilisateur', EntityType::class, [
                    "class" => Utilisateur::class,
                    "choice_label" => "email",
                    "attr" => [
                        "class" => "select2",
                    ],
                    "label" => false,
                    "query_builder" => function (UtilisateurRepository $utilisateurRepository) use ($ug) {
                        return $utilisateurRepository->createQueryBuilder('u');
                    },
                ])
                ;
            }
        });
        
        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => UtilisateurGroupe::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'utilisateurgroupe';
    }

}
