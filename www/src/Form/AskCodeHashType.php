<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\DataClass\AskCodeHash;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AskCodeHashType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', PasswordType::class, [
                'required' => true,
                'attr' => [
                ]
            ])
            ->add('recaptchaToken', HiddenType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'recaptcha_token',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AskCodeHash::class,
        ]);
        
    }
    
   
}
