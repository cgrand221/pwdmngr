<?php

namespace App\Form;
use Symfony\Component\Form\AbstractType;
use App\DataClass\InfoPartageeClair;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class InfoPartageeClairType  extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('libelle', TextType::class, [
                'label' => "Libellé",
            ])
            ->add('login', TextType::class, [
                'label' => "Identifiant",
            ])
            ->add('password', TextType::class, [ 
                'label' => "Mot de passe",
                'attr' => [
                    'class' => 'edit-password',
                    
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => InfoPartageeClair::class,
        ]);
    }
}
