<?php

namespace App\Form;

use App\Entity\InfoPartagee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Groupe;
use App\Entity\Utilisateur;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\InfoPartageeGroupe;

class InfoPartageeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $utilisateur = $options['utilisateur'];
        $builder
                
                ->add('infoPartageeGroupes', CollectionType::class, [
                    "label" => "Groupes",
                    "entry_type" => InfoPartageeGroupeType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'delete_empty' => function (InfoPartageeGroupe $infoPartageeGroupe = null) {
                        return (null === $infoPartageeGroupe) || (null === $infoPartageeGroupe->getGroupe());
                    },
                    'attr' => [
                        "data-delete-button" => "",
                        "data-add-button" => "Ajouter un groupe",
                    ],
                    "entry_options" => [
                        "utilisateur" => $utilisateur
                    ]
                ])
                ->add('infoPartageeClair', InfoPartageeClairType::class, [
                    'mapped' => false,
                    'label' => false,
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => InfoPartagee::class,
        ]);
        $resolver->setRequired('utilisateur');
        $resolver->setAllowedTypes('utilisateur', Utilisateur::class);
    }

}
