<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use App\Validator\Constraints\ContainsComplexPassword;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Validator\Constraints\IsRecaptchaValid;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RegistrationFormType extends UtilisateurInfoType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', RepeatedType::class, [
                    'type' => EmailType::class,
                    'required' => true,
                ]);
        $this->completeBuildForm($builder, $options, false);
        $builder
                ->add('recaptchaToken', HiddenType::class, [
                    'attr' => [
                        'class' => 'recaptcha_token',
                    ],
                    'mapped' => false,
                    'constraints' => [
                        new IsRecaptchaValid()
                    ],
                ])
        ;
    }

    

}
