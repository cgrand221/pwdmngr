<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Groupe;
use App\Entity\Utilisateur;
use App\Entity\InfoPartageeGroupe;
use App\Repository\GroupeRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InfoPartageeGroupeType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        /** @var Utilisateur $utilisateur */
        $utilisateur = $options['utilisateur'];

        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($utilisateur) {
            $ipg = $event->getData();
            $form = $event->getForm();

            // checks if the InfoPartageeGroupe object is "new"
            // If no data is passed to the form, the data is "null".
            // This should be considered a existing "InfoPartageeGroupe"
            if ($ipg && null !== $ipg->getId()) {
                $form->add('groupe', EntityType::class, [
                    "class" => Groupe::class,
                    "choice_label" => "nom",
                    "attr" => [
                        "class" => "select2",
                    ],
                    "label" => false,
                    "query_builder" => function (GroupeRepository $groupeRepository) use ($utilisateur, $ipg) {
                        return $groupeRepository->getMesGroupesQB($utilisateur, false, false, true, $ipg->getGroupe());//on autorise seulement le groupe $ipg->getGroupe()
                    },
                ])
                ;
            }
            else{
                 $form->add('groupe', EntityType::class, [
                    "class" => Groupe::class,
                    "choice_label" => "nom",
                    "attr" => [
                        "class" => "select2",
                    ],
                    "label" => false,
                    "query_builder" => function (GroupeRepository $groupeRepository) use ($utilisateur) {
                        return $groupeRepository->getMesGroupesQB($utilisateur, false, false, true);//on autorise seulement les groupes dont l'utilisateur est membre
                    },
                ])
                ;
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => InfoPartageeGroupe::class,
        ]);
        $resolver->setRequired('utilisateur');
        $resolver->setAllowedTypes('utilisateur', Utilisateur::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'Infopartageegroupe';
    }

}
