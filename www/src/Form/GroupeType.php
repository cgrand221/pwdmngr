<?php

namespace App\Form;

use App\Entity\Groupe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\UtilisateurGroupe;
use App\Validator\Constraints\IsNotEmail;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class GroupeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', TextType::class, [
                    'constraints' => [
                        new IsNotEmail(),
                    ],
                ])
                ->add('etat', ChoiceType::class, [
                    "label" => "Visibilité des mots de passes du groupe",
                    "choices" => [
                        "Tous les membres du groupe" => Groupe::ETAT_ACTIF,
                        "Les créateurs de mots de passes de ce groupe et vous uniquement" => Groupe::ETAT_INACTIF,
                    ]
                ])
                ->add('utilisateurGroupes', CollectionType::class, [
                    "label" => "Utilisateurs",
                    "entry_type" => UtilisateurGroupeType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'delete_empty' => function (UtilisateurGroupe $utilisateurGroupe = null) {
                        return (null === $utilisateurGroupe) || (null === $utilisateurGroupe->getUtilisateur());
                    },
                    'attr' => [
                        "data-delete-button" => "",
                        "data-add-button" => "Ajouter un utilisateur",
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Groupe::class,
        ]);
    }

}
