<?php

namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Utilisateur;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use App\Validator\Constraints\ContainsComplexPassword;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UtilisateurInfoType extends AbstractType {

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     * @param bool $allowEmptyPassword
     */
    public function completeBuildForm(FormBuilderInterface $builder, array $options, $allowEmptyPassword) {
        $constraintsPlainPassword=[
            new ContainsComplexPassword()
        ];
        if($allowEmptyPassword){
            $constraintsPlainPassword[] = new Length([
                // max length allowed by Symfony for security reasons
                'max' => 4096,
            ]);
        }
        else{
            $constraintsPlainPassword[] = new NotBlank([
                'message' => 'Veuillez entrer un mot de passe',
            ]);
            $constraintsPlainPassword[] = new Length([
                'min' => 15,
                'minMessage' => 'Votre mot de passe doit contenir {{ limit }} caractères minimum',
                // max length allowed by Symfony for security reasons
                'max' => 4096,
            ]);
        }
        $builder
                ->add('nom', TextType::class, [
                    'required' => true,
                ])
                ->add('prenom', TextType::class, [
                    'required' => true,
                ])
                ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    // instead of being set onto the object directly,
                    // this is read and encoded in the controller
                    'mapped' => false,
                    'constraints' => $constraintsPlainPassword,
                    'required' => !$allowEmptyPassword,
                ])
                
        ;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
         $this->completeBuildForm($builder, $options, true);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
