<?php
namespace App\DataClass;

use App\Validator\Constraints\ContainsComplexPassword;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;

class PlainPassword{
    /**
     * @ContainsComplexPassword
     * @NotNull
     * @NotBlank
     * @var string 
     */
    private $password;
    
    function getPassword() {
        return $this->password;
    }

    function setPassword($password) {
        $this->password = $password;
    }


}
