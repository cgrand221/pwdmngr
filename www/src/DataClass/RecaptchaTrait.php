<?php
namespace App\DataClass;
use App\Validator\Constraints\IsRecaptchaValid;

trait RecaptchaTrait{
    /**
     * @var string $recaptchaToken 
     * @IsRecaptchaValid
     */
    private $recaptchaToken;
    
    /**
     * @return string
     */
    function getRecaptchaToken() {
        return $this->recaptchaToken;
    }
    
    /**
     * @param string $email
     * @return $this
     */
    function setRecaptchaToken($recaptchaToken) {
        $this->recaptchaToken = $recaptchaToken;
        return $this;
    }

}
