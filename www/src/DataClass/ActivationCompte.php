<?php
namespace App\DataClass;

use App\Validator\Constraints\IsGoodCodeActivation;

/**
 * @IsGoodCodeActivation
 */
class ActivationCompte{
    use RecaptchaTrait;
    
    /**
     *
     * @var string $email 
     */
    private $email;
    
    /**
     *
     * @var string $code
     */
    private $code;
    
    // on ne gere pas l'expiration ici on la gere directement dans la requete UtilisateurRepository::findForActivation
    
    /**
     * @return string
     */
    function getEmail() {
        return $this->email;
    }
    
    /**
     * @return string
     */
    function getCode() {
        return $this->code;
    }
    
    /**
     * @param string $email
     * @return $this
     */
    function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    
    /**
     * @param string $code
     * @return $this
     */
    function setCode($code) {
        $this->code = $code;
        return $this;
    }

   


}
