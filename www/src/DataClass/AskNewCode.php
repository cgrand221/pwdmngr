<?php
namespace App\DataClass;

use App\Validator\Constraints\CanAskCodeActivation;
use App\Validator\Constraints\IsRecaptchaValid;

/**
 * @CanAskCodeActivation
 */
class AskNewCode{
    use RecaptchaTrait;
    /**
     *
     * @var string $email 
     */
    private $email;
    
   
    
    /**
     * @return string
     */
    function getEmail() {
        return $this->email;
    }
    
    /**
     * @param string $email
     * @return $this
     */
    function setEmail($email) {
        $this->email = $email;
        return $this;
    }
   
    
}
