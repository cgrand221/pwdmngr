<?php

namespace App\DataClass;

class InfoPartageeClair {
    /**
     * @var string|null $libelle
     */
    private $libelle;
    
    /**
     * @var string|null $urlLogin
     */
    private $urlLogin;
    
    /**
     * @var string|null $xpathLogin
     */
    private $xpathLogin;
    
    /**
     * @var string|null $login
     */
    private $login;
    
    /**
     * @var string|null $urlPassword
     */
    private $urlPassword;
    
    /**
     * @var string|null $xpathPassword
     */
    private $xpathPassword;
    
    /**
     * @var string|null $password
     */
    private $password;
    
    function getLibelle() {
        return $this->libelle;
    }

    function getUrlLogin() {
        return $this->urlLogin;
    }

    function getXpathLogin() {
        return $this->xpathLogin;
    }

    function getLogin() {
        return $this->login;
    }

    function getUrlPassword() {
        return $this->urlPassword;
    }

    function getXpathPassword() {
        return $this->xpathPassword;
    }

    function getPassword() {
        return $this->password;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setUrlLogin($urlLogin) {
        $this->urlLogin = $urlLogin;
    }

    function setXpathLogin($xpathLogin) {
        $this->xpathLogin = $xpathLogin;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setUrlPassword($urlPassword) {
        $this->urlPassword = $urlPassword;
    }

    function setXpathPassword($xpathPassword) {
        $this->xpathPassword = $xpathPassword;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    /**
     * @param InfoPartageeClair $ipc
     * @return bool
     */
    public function equals(InfoPartageeClair $ipc){
        return (
            ($this->getLibelle() == $ipc->getLibelle())
            && ($this->getUrlLogin() == $ipc->getUrlLogin())
            && ($this->getXpathLogin() == $ipc->getXpathLogin())
            && ($this->getLogin() == $ipc->getLogin())
            && ($this->getUrlPassword() == $ipc->getUrlPassword())
            && ($this->getXpathPassword() == $ipc->getXpathPassword())
            && ($this->getPassword() == $ipc->getPassword())
        );
    }
    
}
