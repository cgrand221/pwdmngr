<?php
namespace App\DataClass;


use App\Validator\Constraints\IsRecaptchaValid;
use App\Validator\Constraints\NotExpiration;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Email;
use App\Validator\Constraints\IsGoodNewMail;


class AskNewMail{
    use RecaptchaTrait;
    /**
     * @NotNull
     * @Email
     * @IsGoodNewMail
     * @var string|null $code
     */
    private $email;
    
    /**
     * @NotNull
     * @NotExpiration
     * @var \DateTime|null $expiration
     */
    private $expiration;
    
    function getEmail() {
        return $this->email;
    }

    function getExpiration(): \DateTime {
        return $this->expiration;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setExpiration(\DateTime $expiration) {
        $this->expiration = $expiration;
    }


    
}
