<?php
namespace App\DataClass;

use App\Validator\Constraints\IsGoodAskCodeHash;
use App\Validator\Constraints\NotExpiration;
use Symfony\Component\Validator\Constraints\NotNull;
/**
 * @IsGoodAskCodeHash
 */
class AskCodeHash{
    use RecaptchaTrait;
    /**
     * @NotNull
     * @var string $hash
     */
    private $hash;
    
    /**
     * @NotNull
     * @var string|null $code
     */
    private $code;
    
    /**
     * @NotNull
     * @NotExpiration
     * @var \DateTime|null $expiration
     */
    private $expiration;
    
    /**
     * @return string
     */
    function getHash() {
        return $this->hash;
    }

    /**
     * @return string
     */
    function getCode() {
        return $this->code;
    }

    /**
     * @param string $hash
     * @return $this
     */
    function setHash($hash) {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @param string $code
     * @return $this
     */
    function setCode($code) {
        $this->code = $code;
        return $this;
    }
    
    /**
     * @return \DateTime|null
     */
    function getExpiration() {
        return $this->expiration;
    }

    /**
     * @param \DateTime|null $expiration
     * @return $this
     */
    function setExpiration($expiration) {
        $this->expiration = $expiration;
        return $this;
    }




}
