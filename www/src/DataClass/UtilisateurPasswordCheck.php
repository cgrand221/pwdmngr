<?php


namespace App\DataClass;
use App\Entity\Utilisateur;
use App\Validator\Constraints\IsGoodPasswordCheck;
/**
 * @IsGoodPasswordCheck
 */
class UtilisateurPasswordCheck {
    use RecaptchaTrait;
    
    /**
     * @var string $password
     */
    private $password;
    
     /**
     * @var Utilisateur $utilisateur
     */
    private $utilisateur;
    
    function getPassword() {
        return $this->password;
    }

    function setPassword($password) {
        $this->password = $password;
    }
    
    function getUtilisateur(): Utilisateur {
        return $this->utilisateur;
    }

    function setUtilisateur(Utilisateur $utilisateur) {
        $this->utilisateur = $utilisateur;
    }



}
