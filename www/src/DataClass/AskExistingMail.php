<?php
namespace App\DataClass;

use App\Validator\Constraints\IsRecaptchaValid;
use Symfony\Component\Validator\Constraints\Email;
use App\Validator\Constraints\IsGoodExistingMail;
use Symfony\Component\Validator\Constraints\NotNull;


class AskExistingMail{
    use RecaptchaTrait;
    /**
     * @NotNull
     * @Email
     * @IsGoodExistingMail
     * @var string|null $code
     */
    private $email;
    
    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

}
