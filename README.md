

Password MANAGER
================

Permet de stocker/partager/générer vos mot de passe en toute sécurité

1er demarrage:
--------------

```
git clone git@gitlab.com:cgrand221/pwdmngr.git
cd pwdmngr
make build
```

Dans un 2e terminal, entrez les commandes:
```
cd pwdmngr
make bash_apache_php
/root/bin/composer install
yarn install
```

Créez le fichier pwdmngr/www/.env.local en faisant une copie du fichier pwdmngr/www/.env pour pouvoir surcharger les variable d'environnement.

Modifiez si besoin (prod) le fichier pwdmngr/www/.env.local pour pouvoir se connecter à la base de données :
```
DATABASE_URL=...
```

* Connectez-vous à localhost:8080
* Créez une base de donnée pswdmng 

Dans le 2e terminal, entrez les commandes:
```
bin/console doctrine:migrations:migrate
```

Modifiez le fichier /etc/hosts de votre machine physique et insérez le contenu :

```
127.0.0.1       www.local-password-manager.fr
```

Demarrages suivants:
--------------------

```
cd pwdmngr
make up
```

Utilisation après demarrage :
-----------------------------

Dans un 2e terminal :
```
cd pwdmngr
make bash_apache_php
yarn encore dev --watch
```

Arreter :
---------

Dans un 3e terminal :

```
make down
```



Activer le recaptcha :
----------------------

* Générez des clefs à partir de votre compte google
* Renseignez ces clefs dans le fichier pwdmngr/www/.env.local : 
```
###> recaptcha secret ###
RECAPTCHA_SECRET_KEY=...
RECAPTCHA_PUBLIC_KEY=...
###< symfony/swiftmailer-bundle ###

```
Editez ensuite la configuration dans le fichier pwdmngr/www/config/services.yaml :
```
app.use_recaptcha: true
```

Crédits :
---------

* template crew téléchargé sur http://freehtml5.co/
* images : 
    * https://unsplash.com/
        * Photo by Kaitlyn Baker on Unsplash
        * Photo by John Salvino on Unsplash
    * http://pexels.com/
    

