up:
	docker-compose up

build:
	docker-compose up --build

down:
	docker-compose down

bash_mysql:
	docker exec -w /var/lib/mysql -it passwordmanager_db_1 bash -l

bash_apache_php:
	docker exec -w /var/www/passwordManager/ -it passwordmanager_web_1 bash 
