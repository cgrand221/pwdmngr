
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.1] - 2019-03-24
- Correction du fichier README.md : ajout du nom de domaine local

## [0.3.0] - 2019-03-24
### Changements
- Méthode de dérivation du mot de passe de l'utilisateur : utilisation de la fonction hash_pbkdf2 pour plus de sécurité et ne pas avoir un echec de test du à une fonction dépréciée
### Ajout
- Tests unitaires de base

## [0.2.0] - 2019-03-24
### Ajout
- readme
- changelog

## [0.1.0] - 2019-03-24
### Ajout
- Logique générale de l'application
    - Présentation des fonctionnalités en front
    - Création de compte
    - Création/modification de groupe
    - Création/modification de mots de passe
    - Modication de l'utilisateur nom/prénom/mot de passe principale/email
    - Récupération de compte
    - Générateur de mots de passe
    - Envois de mails avec code pour la création de compte, changement de mot de passe, récupération de compte, à la modification de mail
    - Temps limte d'utilisation du code envoyé par mail
    - ajout du recaptcha (activation configurable) dans divers partie critique du site, ou les attaques par force brute ne doivent pas être utilisées
    - Ajout du copié javascript
